-- "CP".dual definition

-- Drop table

-- DROP TABLE "CP".dual;

CREATE TABLE "CP".dual (
	col bpchar(1) NULL
);


-- "CP".icp_decision_master definition

-- Drop table

-- DROP TABLE "CP".icp_decision_master;

CREATE TABLE "CP".icp_decision_master (
	decision_code varchar(50) NOT NULL,
	decision_label varchar(50) NOT NULL,
	superlative_label varchar(20) NULL,
	inferior_label varchar(20) NULL,
	scoring_erule_code varchar(50) NOT NULL,
	scoring_display_erule varchar(50) NOT NULL,
	commonality_factor numeric(20,10) NOT NULL,
	directly_proportional varchar(1) NULL,
	rule_bucket varchar(50) NOT NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_decision_master_pkey PRIMARY KEY (decision_code)
);


-- "CP".icp_downstream_audit definition

-- Drop table

-- DROP TABLE "CP".icp_downstream_audit;

CREATE TABLE "CP".icp_downstream_audit (
	alu_audit_id serial NOT NULL,
	api_name varchar(50) NULL,
	api_offering_code varchar(50) NULL,
	api_cp_reference_id varchar(50) NULL,
	api_channel_request_id varchar(50) NULL,
	api_date_time timestamp NULL,
	api_system_code varchar(20) NULL,
	api_req_resp varchar(10) NULL,
	api_uri varchar(2000) NULL,
	api_header varchar(2000) NULL,
	api_body varchar(20000) NULL,
	alu_inserted_time timestamp NULL,
	CONSTRAINT icp_downstream_audit_pkey PRIMARY KEY (alu_audit_id)
);


-- "CP".icp_dynamic_api definition

-- Drop table

-- DROP TABLE "CP".icp_dynamic_api;

CREATE TABLE "CP".icp_dynamic_api (
	api_code varchar(50) NOT NULL,
	system_code varchar(20) NOT NULL,
	api_name varchar(50) NOT NULL,
	ssl_enabled varchar(1) NOT NULL,
	secret_key_field varchar(20) NULL,
	http_uri varchar(2000) NOT NULL,
	http_method varchar(10) NOT NULL,
	is_body_templatized varchar(1) NOT NULL,
	body_template varchar(4000) NULL,
	single_row varchar(1) NOT NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_dynamic_api_pkey PRIMARY KEY (system_code, api_name)
);


-- "CP".icp_dynamic_api_dtls definition

-- Drop table

-- DROP TABLE "CP".icp_dynamic_api_dtls;

CREATE TABLE "CP".icp_dynamic_api_dtls (
	api_param_pk serial NOT NULL,
	api_code varchar(50) NOT NULL,
	req_resp varchar(5) NOT NULL,
	param_type varchar(10) NULL,
	seq numeric(5) NULL,
	param_name varchar(50) NULL,
	param_field_code varchar(50) NULL,
	param_static_value varchar(50) NULL,
	resp_body_json_path varchar(1000) NULL,
	resp_body_json_field varchar(50) NULL,
	CONSTRAINT icp_dynamic_api_dtls_pkey PRIMARY KEY (api_param_pk)
);


-- "CP".icp_error_codes definition

-- Drop table

-- DROP TABLE "CP".icp_error_codes;

CREATE TABLE "CP".icp_error_codes (
	error_code varchar(50) NOT NULL,
	error_category varchar(20) NOT NULL,
	error_description_def varchar(200) NOT NULL,
	cust_error_code varchar(20) NULL,
	cust_error_description_def varchar(200) NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_error_codes_pkey PRIMARY KEY (error_code)
);


-- "CP".icp_erule_dtls definition

-- Drop table

-- DROP TABLE "CP".icp_erule_dtls;

CREATE TABLE "CP".icp_erule_dtls (
	erule_dtls_pk serial NOT NULL,
	erule_code varchar(50) NOT NULL,
	seq numeric(5) NOT NULL,
	vrule_bucket varchar(20) NOT NULL,
	vrule_code varchar(50) NOT NULL,
	if_enrichment_type varchar(50) NOT NULL,
	if_enrichment_value varchar(200) NOT NULL,
	CONSTRAINT icp_erule_dtls_pkey PRIMARY KEY (erule_dtls_pk)
);


-- "CP".icp_erule_master definition

-- Drop table

-- DROP TABLE "CP".icp_erule_master;

CREATE TABLE "CP".icp_erule_master (
	erule_code varchar(50) NOT NULL,
	erule_name varchar(50) NOT NULL,
	erule_description varchar(200) NULL,
	rule_bucket varchar(20) NOT NULL,
	else_enrichment_type varchar(20) NULL,
	else_enrichment_value varchar(50) NULL,
	is_system_defined varchar(1) NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_erule_master_pkey PRIMARY KEY (erule_code)
);


-- "CP".icp_exception_log definition

-- Drop table

-- DROP TABLE "CP".icp_exception_log;

CREATE TABLE "CP".icp_exception_log (
	alu_audit_id serial NOT NULL,
	api_cp_reference_id varchar(50) NULL,
	api_channel_request_id varchar(50) NULL,
	api_date_time timestamp NULL,
	api_layer varchar(50) NULL,
	api_stage varchar(50) NULL,
	api_fine_grain varchar(50) NULL,
	api_payload varchar(4000) NULL,
	api_error_msg varchar(4000) NULL,
	api_error_trace varchar(4000) NULL,
	CONSTRAINT icp_exception_log_pkey PRIMARY KEY (alu_audit_id)
);


-- "CP".icp_field_master definition

-- Drop table

-- DROP TABLE "CP".icp_field_master;

CREATE TABLE "CP".icp_field_master (
	field_code varchar(50) NOT NULL,
	field_name varchar(50) NOT NULL,
	field_bucket varchar(20) NOT NULL,
	data_type varchar(20) NOT NULL,
	is_array varchar(1) NOT NULL,
	data_length varchar(4) NULL,
	vrule_code varchar(50) NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_field_master_pkey PRIMARY KEY (field_code)
);


-- "CP".icp_interfacing_system definition

-- Drop table

-- DROP TABLE "CP".icp_interfacing_system;

CREATE TABLE "CP".icp_interfacing_system (
	system_code varchar(20) NOT NULL,
	system_name varchar(50) NOT NULL,
	upstream_downstream varchar(1) NOT NULL,
	addr_disc_type varchar(10) NULL,
	discovery_server_name varchar(200) NULL,
	dns_name varchar(200) NULL,
	port varchar(20) NULL,
	is_system_defined varchar(1) NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_interfacing_system_pkey PRIMARY KEY (system_name)
);


-- "CP".icp_list_config definition

-- Drop table

-- DROP TABLE "CP".icp_list_config;

CREATE TABLE "CP".icp_list_config (
	list_code varchar(50) NOT NULL,
	list_name varchar(50) NOT NULL,
	list_source varchar(20) NOT NULL,
	list_pk varchar(20) NULL,
	query_sql varchar(20) NULL,
	api_code varchar(20) NULL,
	is_system_defined varchar(1) NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_list_config_pkey PRIMARY KEY (list_code)
);


-- "CP".icp_list_config_dtls definition

-- Drop table

-- DROP TABLE "CP".icp_list_config_dtls;

CREATE TABLE "CP".icp_list_config_dtls (
	list_pk varchar(50) NOT NULL DEFAULT nextval('"CP".icp_list_config_dtls_list_pk_seq'::regclass),
	list_code varchar(50) NOT NULL,
	list_seq numeric(5) NOT NULL,
	list_name varchar(20) NOT NULL,
	list_value varchar(30) NOT NULL,
	CONSTRAINT icp_list_config_dtls_pkey PRIMARY KEY (list_code, list_seq)
);


-- "CP".icp_offering_decision definition

-- Drop table

-- DROP TABLE "CP".icp_offering_decision;

CREATE TABLE "CP".icp_offering_decision (
	decision_code_pk int4 NOT NULL,
	decision_code varchar(50) NOT NULL,
	offering_code varchar(50) NOT NULL,
	decision_opted varchar(1) NOT NULL,
	CONSTRAINT icp_offering_decision_pkey PRIMARY KEY (decision_code_pk)
);


-- "CP".icp_offering_elimination definition

-- Drop table

-- DROP TABLE "CP".icp_offering_elimination;

CREATE TABLE "CP".icp_offering_elimination (
	offering_rule_pk serial NOT NULL,
	offering_code varchar(50) NOT NULL,
	vrule_code varchar(50) NOT NULL,
	raise_error varchar(1) NOT NULL,
	error_code varchar(50) NULL,
	CONSTRAINT icp_offering_elimination_pkey PRIMARY KEY (offering_rule_pk)
);


-- "CP".icp_offering_enrichment definition

-- Drop table

-- DROP TABLE "CP".icp_offering_enrichment;

CREATE TABLE "CP".icp_offering_enrichment (
	offering_enrich_pk serial NOT NULL,
	offering_code varchar(50) NOT NULL,
	offering_field_code varchar(50) NOT NULL,
	enrich_seq numeric(5) NOT NULL,
	erule_code varchar(50) NOT NULL,
	raise_error varchar(1) NOT NULL,
	vrule_code varchar(50) NULL,
	error_code varchar(50) NULL,
	CONSTRAINT icp_offering_enrichment_pkey PRIMARY KEY (offering_enrich_pk)
);


-- "CP".icp_offering_fields definition

-- Drop table

-- DROP TABLE "CP".icp_offering_fields;

CREATE TABLE "CP".icp_offering_fields (
	offering_field_pk serial NOT NULL,
	offering_code varchar(50) NOT NULL,
	offering_field_code varchar(50) NOT NULL,
	field_bucket varchar(20) NOT NULL,
	is_mandatory varchar(1) NOT NULL,
	input_derived_fulfilment varchar(1) NULL,
	CONSTRAINT icp_offering_fields_pkey PRIMARY KEY (offering_field_pk)
);


-- "CP".icp_offering_pref_impl definition

-- Drop table

-- DROP TABLE "CP".icp_offering_pref_impl;

CREATE TABLE "CP".icp_offering_pref_impl (
	offering_code varchar(50) NOT NULL,
	seq numeric(5) NOT NULL,
	is_system_defined varchar(2) NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL
);


-- "CP".icp_parameter definition

-- Drop table

-- DROP TABLE "CP".icp_parameter;

CREATE TABLE "CP".icp_parameter (
	parameter_pk int4 NOT NULL,
	param_type varchar(50) NOT NULL,
	param_sub_type varchar(50) NOT NULL,
	param_key varchar(50) NULL,
	param_value varchar(500) NULL,
	param_order numeric(5) NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_parameter_pkey PRIMARY KEY (parameter_pk)
);


-- "CP".icp_pref_details definition

-- Drop table

-- DROP TABLE "CP".icp_pref_details;

CREATE TABLE "CP".icp_pref_details (
	hierarchy_details_pk serial NOT NULL,
	hierarchy_master_pk int4 NOT NULL,
	decision_param_code varchar(20) NOT NULL,
	pref_score_multiplier varchar NOT NULL,
	CONSTRAINT icp_pref_details_hierarchy_master_pk_decision_param_code_key UNIQUE (hierarchy_master_pk, decision_param_code),
	CONSTRAINT icp_pref_details_pkey PRIMARY KEY (hierarchy_details_pk)
);


-- "CP".icp_pref_hierarchy_impl definition

-- Drop table

-- DROP TABLE "CP".icp_pref_hierarchy_impl;

CREATE TABLE "CP".icp_pref_hierarchy_impl (
	pref_hierarchy_pk int4 NOT NULL,
	hierarchy_entity_type varchar(20) NOT NULL,
	hierarchy_level numeric(5) NOT NULL,
	field_code varchar(50) NOT NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_pref_hierarchy_impl_pkey PRIMARY KEY (pref_hierarchy_pk)
);


-- "CP".icp_pref_master definition

-- Drop table

-- DROP TABLE "CP".icp_pref_master;

CREATE TABLE "CP".icp_pref_master (
	hierarchy_master_pk serial NOT NULL,
	hierarchy_entity_type varchar(20) NOT NULL,
	hierarchy_entity_code varchar(50) NOT NULL,
	disclaimer_bucket varchar(50) NULL,
	advise_bucket varchar(50) NULL,
	marketing_bucket varchar(50) NULL,
	disclaimer_erule varchar(20) NULL,
	advise_erule varchar(20) NULL,
	marketing_erule varchar(20) NULL,
	is_system_defined varchar(1) NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_pref_master_pkey PRIMARY KEY (hierarchy_master_pk)
);


-- "CP".icp_processing_audit definition

-- Drop table

-- DROP TABLE "CP".icp_processing_audit;

CREATE TABLE "CP".icp_processing_audit (
	alu_audit_id serial NOT NULL,
	api_cp_reference_id varchar(50) NULL,
	api_channel_request_id varchar(50) NULL,
	api_date_time timestamp NULL,
	api_layer varchar(50) NULL,
	api_stage varchar(50) NULL,
	api_fine_grain varchar(50) NULL,
	api_offering_code varchar(50) NULL,
	api_decision_param varchar(50) NULL,
	api_field_code varchar(50) NULL,
	api_vrule_code varchar(50) NULL,
	api_erule_code varchar(50) NULL,
	api_payload varchar(20000) NULL,
	api_status varchar(50) NULL,
	alu_inserted_time timestamp NULL,
	api_value varchar NULL,
	CONSTRAINT icp_processing_audit_pkey PRIMARY KEY (alu_audit_id)
);


-- "CP".icp_product_offering definition

-- Drop table

-- DROP TABLE "CP".icp_product_offering;

CREATE TABLE "CP".icp_product_offering (
	offering_code varchar(50) NOT NULL,
	entity_code varchar(50) NOT NULL,
	product_code varchar(50) NOT NULL,
	sub_product_code varchar(50) NOT NULL,
	offering_name varchar(50) NOT NULL,
	offering_description varchar(50) NULL,
	disclaimer_erule varchar(50) NULL,
	advise_erule varchar(50) NULL,
	marketing_erule varchar(50) NULL,
	iteration_count numeric(1) NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_product_offering_pkey PRIMARY KEY (offering_code)
);


-- "CP".icp_query_config definition

-- Drop table

-- DROP TABLE "CP".icp_query_config;

CREATE TABLE "CP".icp_query_config (
	sql_code varchar(50) NOT NULL,
	sql_name varchar(50) NOT NULL,
	sql_descrition varchar(200) NOT NULL,
	sql_type varchar(10) NOT NULL,
	sql_datasource varchar(50) NOT NULL,
	query_string varchar(4000) NOT NULL,
	response_alias varchar(30) NOT NULL,
	single_row varchar(1) NOT NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_query_config_pkey PRIMARY KEY (sql_name)
);


-- "CP".icp_query_config_input definition

-- Drop table

-- DROP TABLE "CP".icp_query_config_input;

CREATE TABLE "CP".icp_query_config_input (
	sql_input_pk serial NOT NULL,
	sql_code varchar(50) NOT NULL,
	seq numeric(5) NOT NULL,
	param_datatype varchar(1) NULL,
	param_field_code varchar(20) NOT NULL,
	CONSTRAINT icp_query_config_input_pkey PRIMARY KEY (sql_code, seq)
);


-- "CP".icp_recommendation_audit definition

-- Drop table

-- DROP TABLE "CP".icp_recommendation_audit;

CREATE TABLE "CP".icp_recommendation_audit (
	alu_audit_id serial NOT NULL,
	api_gr_cp_reference_id varchar(50) NULL,
	api_gr_channel_request_id varchar(50) NULL,
	api_sr_channel_request_id varchar(50) NULL,
	api_date_time timestamp NULL,
	api_body varchar(20000) NULL,
	api_cp_recommendation varchar(50) NULL,
	api_user_selection varchar(50) NULL,
	alu_inserted_time timestamp NULL,
	CONSTRAINT icp_recommendation_audit_pkey PRIMARY KEY (alu_audit_id)
);


-- "CP".icp_upstream_audit definition

-- Drop table

-- DROP TABLE "CP".icp_upstream_audit;

CREATE TABLE "CP".icp_upstream_audit (
	alu_audit_id serial NOT NULL,
	api_name varchar(50) NULL,
	api_cp_reference_id varchar(50) NULL,
	api_channel_request_id varchar(50) NULL,
	api_orig_channel_request_id varchar(50) NULL,
	api_date_time timestamp NULL,
	api_system_code varchar(20) NULL,
	api_req_resp varchar(10) NULL,
	api_uri varchar(2000) NULL,
	api_header varchar(2000) NULL,
	api_body varchar(20000) NULL,
	alu_inserted_time timestamp NULL,
	CONSTRAINT icp_upstream_audit_pkey PRIMARY KEY (alu_audit_id)
);


-- "CP".icp_vrule_dtls definition

-- Drop table

-- DROP TABLE "CP".icp_vrule_dtls;

CREATE TABLE "CP".icp_vrule_dtls (
	vrule_dtls_pk serial NOT NULL,
	vrule_code varchar(50) NOT NULL,
	field_code varchar(50) NULL,
	"operator" varchar(50) NOT NULL,
	negate varchar(1) NOT NULL,
	comparator varchar(50) NULL,
	value_type varchar(50) NULL,
	value varchar(50) NULL,
	vrule_and_seq varchar(20) NOT NULL,
	CONSTRAINT icp_vrule_dtls_pkey PRIMARY KEY (vrule_dtls_pk)
);


-- "CP".icp_vrule_master definition

-- Drop table

-- DROP TABLE "CP".icp_vrule_master;

CREATE TABLE "CP".icp_vrule_master (
	vrule_code varchar(50) NOT NULL,
	vrule_name varchar(50) NOT NULL,
	vrule_description varchar(200) NOT NULL,
	vrule_bucket varchar(20) NOT NULL,
	vrule_type varchar(1) NOT NULL,
	is_system_defined varchar(1) NOT NULL,
	maker_id varchar(50) NULL,
	maker_date timestamp NULL,
	checker_id varchar(50) NULL,
	checker_date timestamp NULL,
	flag_closed varchar(1) NULL,
	CONSTRAINT icp_vrule_master_pkey PRIMARY KEY (vrule_name)
);


-- "CP".sp_transaction_master definition

-- Drop table

-- DROP TABLE "CP".sp_transaction_master;

CREATE TABLE "CP".sp_transaction_master (
	id_channel_reqid varchar(100) NOT NULL,
	id_contextual_refid varchar(100) NOT NULL,
	in_dr_ac varchar(100) NOT NULL,
	in_dr_ac_ccy varchar(100) NOT NULL,
	in_cr_agt_ctry varchar(100) NOT NULL,
	in_val_dt varchar(100) NULL,
	in_inst_amt numeric(100,50) NOT NULL,
	in_inst_amt_ccy varchar(100) NOT NULL,
	in_debt_amt numeric(100,50) NOT NULL,
	in_debt_amt_ccy varchar(100) NOT NULL,
	fl_ntnl_rate varchar(100) NOT NULL,
	in_cust_id varchar(100) NULL,
	offering_code_recommended varchar(100) NOT NULL,
	offering_code_selected varchar(100) NOT NULL,
	decision_score_cost numeric(100,50) NOT NULL,
	decision_score_time numeric(100,50) NOT NULL,
	decision_score_display_cost varchar(100) NOT NULL,
	decision_score_display_time varchar(100) NOT NULL,
	ful_field1 varchar(100) NOT NULL,
	ful_field2 varchar(100) NOT NULL,
	ful_field3 varchar(100) NULL,
	ful_field4 varchar(100) NULL,
	ful_field5 varchar(100) NULL,
	ful_field6 varchar(100) NULL,
	ful_field7 varchar(100) NULL,
	ful_field8 varchar(100) NULL,
	maker_id varchar(100) NULL,
	maker_date timestamp NULL,
	checker_id varchar(100) NULL,
	checker_date timestamp NULL,
	status varchar(100) NULL,
	CONSTRAINT sp_transaction_master_pkey PRIMARY KEY (id_contextual_refid)
);