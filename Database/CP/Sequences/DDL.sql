-- "CP".icp_downstream_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP".icp_downstream_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP".icp_downstream_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_dynamic_api_dtls_api_param_pk_seq definition

-- DROP SEQUENCE "CP".icp_dynamic_api_dtls_api_param_pk_seq;

CREATE SEQUENCE "CP".icp_dynamic_api_dtls_api_param_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_erule_dtls_erule_dtls_pk_seq definition

-- DROP SEQUENCE "CP".icp_erule_dtls_erule_dtls_pk_seq;

CREATE SEQUENCE "CP".icp_erule_dtls_erule_dtls_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_exception_log_alu_audit_id_seq definition

-- DROP SEQUENCE "CP".icp_exception_log_alu_audit_id_seq;

CREATE SEQUENCE "CP".icp_exception_log_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1100
	CACHE 1
	NO CYCLE;


-- "CP".icp_list_config_dtls_list_pk_seq definition

-- DROP SEQUENCE "CP".icp_list_config_dtls_list_pk_seq;

CREATE SEQUENCE "CP".icp_list_config_dtls_list_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_offering_elimination_offering_rule_pk_seq definition

-- DROP SEQUENCE "CP".icp_offering_elimination_offering_rule_pk_seq;

CREATE SEQUENCE "CP".icp_offering_elimination_offering_rule_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_offering_enrichment_offering_enrich_pk_seq definition

-- DROP SEQUENCE "CP".icp_offering_enrichment_offering_enrich_pk_seq;

CREATE SEQUENCE "CP".icp_offering_enrichment_offering_enrich_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_offering_fields_offering_field_pk_seq definition

-- DROP SEQUENCE "CP".icp_offering_fields_offering_field_pk_seq;

CREATE SEQUENCE "CP".icp_offering_fields_offering_field_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_pref_details_hierarchy_details_pk_seq definition

-- DROP SEQUENCE "CP".icp_pref_details_hierarchy_details_pk_seq;

CREATE SEQUENCE "CP".icp_pref_details_hierarchy_details_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_pref_master_hierarchy_master_pk_seq definition

-- DROP SEQUENCE "CP".icp_pref_master_hierarchy_master_pk_seq;

CREATE SEQUENCE "CP".icp_pref_master_hierarchy_master_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_processing_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP".icp_processing_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP".icp_processing_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_query_config_input_sql_input_pk_seq definition

-- DROP SEQUENCE "CP".icp_query_config_input_sql_input_pk_seq;

CREATE SEQUENCE "CP".icp_query_config_input_sql_input_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_recommendation_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP".icp_recommendation_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP".icp_recommendation_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1299
	CACHE 1
	NO CYCLE;


-- "CP".icp_upstream_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP".icp_upstream_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP".icp_upstream_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP".icp_vrule_dtls_vrule_dtls_pk_seq definition

-- DROP SEQUENCE "CP".icp_vrule_dtls_vrule_dtls_pk_seq;

CREATE SEQUENCE "CP".icp_vrule_dtls_vrule_dtls_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1001
	CACHE 1
	NO CYCLE;