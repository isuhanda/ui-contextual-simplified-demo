CREATE UNIQUE INDEX icp_decision_master_pkey ON "CP_Client".icp_decision_master USING btree (decision_code);

CREATE UNIQUE INDEX icp_downstream_audit_pkey ON "CP_Client".icp_downstream_audit USING btree (alu_audit_id);

CREATE UNIQUE INDEX icp_dynamic_api_dtls_pkey ON "CP_Client".icp_dynamic_api_dtls USING btree (api_param_pk);

CREATE UNIQUE INDEX icp_dynamic_api_pkey ON "CP_Client".icp_dynamic_api USING btree (system_code, api_name);

CREATE UNIQUE INDEX icp_error_codes_pkey ON "CP_Client".icp_error_codes USING btree (error_code);

CREATE UNIQUE INDEX icp_erule_dtls_pkey ON "CP_Client".icp_erule_dtls USING btree (erule_dtls_pk);

CREATE UNIQUE INDEX icp_erule_master_pkey ON "CP_Client".icp_erule_master USING btree (erule_code);

CREATE UNIQUE INDEX icp_exception_log_pkey ON "CP_Client".icp_exception_log USING btree (alu_audit_id);

CREATE UNIQUE INDEX icp_field_master_pkey ON "CP_Client".icp_field_master USING btree (field_code);

CREATE UNIQUE INDEX icp_interfacing_system_pkey ON "CP_Client".icp_interfacing_system USING btree (system_name);

CREATE UNIQUE INDEX icp_list_config_dtls_pkey ON "CP_Client".icp_list_config_dtls USING btree (list_code, list_seq);

CREATE UNIQUE INDEX icp_list_config_pkey ON "CP_Client".icp_list_config USING btree (list_code);

CREATE UNIQUE INDEX icp_offering_decision_pkey ON "CP_Client".icp_offering_decision USING btree (decision_code_pk);

CREATE UNIQUE INDEX icp_offering_elimination_pkey ON "CP_Client".icp_offering_elimination USING btree (offering_rule_pk);

CREATE UNIQUE INDEX icp_offering_enrichment_pkey ON "CP_Client".icp_offering_enrichment USING btree (offering_enrich_pk);

CREATE UNIQUE INDEX icp_offering_fields_pkey ON "CP_Client".icp_offering_fields USING btree (offering_field_pk);

CREATE UNIQUE INDEX icp_parameter_pkey ON "CP_Client".icp_parameter USING btree (parameter_pk);

CREATE UNIQUE INDEX icp_pref_details_hierarchy_master_pk_decision_param_code_key ON "CP_Client".icp_pref_details USING btree (hierarchy_master_pk, decision_param_code);

CREATE UNIQUE INDEX icp_pref_details_pkey ON "CP_Client".icp_pref_details USING btree (hierarchy_details_pk);

CREATE UNIQUE INDEX icp_pref_hierarchy_impl_pkey ON "CP_Client".icp_pref_hierarchy_impl USING btree (pref_hierarchy_pk);

CREATE UNIQUE INDEX icp_pref_master_pkey ON "CP_Client".icp_pref_master USING btree (hierarchy_master_pk);

CREATE UNIQUE INDEX icp_processing_audit_pkey ON "CP_Client".icp_processing_audit USING btree (alu_audit_id);

CREATE UNIQUE INDEX icp_product_offering_pkey ON "CP_Client".icp_product_offering USING btree (offering_code);

CREATE UNIQUE INDEX icp_query_config_input_pkey ON "CP_Client".icp_query_config_input USING btree (sql_code, seq);

CREATE UNIQUE INDEX icp_query_config_pkey ON "CP_Client".icp_query_config USING btree (sql_name);

CREATE UNIQUE INDEX icp_recommendation_audit_pkey ON "CP_Client".icp_recommendation_audit USING btree (alu_audit_id);

CREATE UNIQUE INDEX icp_upstream_audit_pkey ON "CP_Client".icp_upstream_audit USING btree (alu_audit_id);

CREATE UNIQUE INDEX icp_vrule_dtls_pkey ON "CP_Client".icp_vrule_dtls USING btree (vrule_dtls_pk);

CREATE UNIQUE INDEX icp_vrule_master_pkey ON "CP_Client".icp_vrule_master USING btree (vrule_name);

CREATE UNIQUE INDEX sp_transaction_master_pkey ON "CP_Client".sp_transaction_master USING btree (id_contextual_refid);
