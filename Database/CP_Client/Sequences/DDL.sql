-- "CP_Client".icp_downstream_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP_Client".icp_downstream_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP_Client".icp_downstream_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_dynamic_api_dtls_api_param_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_dynamic_api_dtls_api_param_pk_seq;

CREATE SEQUENCE "CP_Client".icp_dynamic_api_dtls_api_param_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_erule_dtls_erule_dtls_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_erule_dtls_erule_dtls_pk_seq;

CREATE SEQUENCE "CP_Client".icp_erule_dtls_erule_dtls_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_exception_log_alu_audit_id_seq definition

-- DROP SEQUENCE "CP_Client".icp_exception_log_alu_audit_id_seq;

CREATE SEQUENCE "CP_Client".icp_exception_log_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1042
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_list_config_dtls_list_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_list_config_dtls_list_pk_seq;

CREATE SEQUENCE "CP_Client".icp_list_config_dtls_list_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_offering_elimination_offering_rule_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_offering_elimination_offering_rule_pk_seq;

CREATE SEQUENCE "CP_Client".icp_offering_elimination_offering_rule_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_offering_enrichment_offering_enrich_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_offering_enrichment_offering_enrich_pk_seq;

CREATE SEQUENCE "CP_Client".icp_offering_enrichment_offering_enrich_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_offering_fields_offering_field_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_offering_fields_offering_field_pk_seq;

CREATE SEQUENCE "CP_Client".icp_offering_fields_offering_field_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_pref_details_hierarchy_details_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_pref_details_hierarchy_details_pk_seq;

CREATE SEQUENCE "CP_Client".icp_pref_details_hierarchy_details_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_pref_master_hierarchy_master_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_pref_master_hierarchy_master_pk_seq;

CREATE SEQUENCE "CP_Client".icp_pref_master_hierarchy_master_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_processing_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP_Client".icp_processing_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP_Client".icp_processing_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_query_config_input_sql_input_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_query_config_input_sql_input_pk_seq;

CREATE SEQUENCE "CP_Client".icp_query_config_input_sql_input_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_recommendation_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP_Client".icp_recommendation_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP_Client".icp_recommendation_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_upstream_audit_alu_audit_id_seq definition

-- DROP SEQUENCE "CP_Client".icp_upstream_audit_alu_audit_id_seq;

CREATE SEQUENCE "CP_Client".icp_upstream_audit_alu_audit_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;


-- "CP_Client".icp_vrule_dtls_vrule_dtls_pk_seq definition

-- DROP SEQUENCE "CP_Client".icp_vrule_dtls_vrule_dtls_pk_seq;

CREATE SEQUENCE "CP_Client".icp_vrule_dtls_vrule_dtls_pk_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	CACHE 1
	NO CYCLE;