-- DROP TYPE "CP_Client"."_dual";

CREATE TYPE "CP_Client"."_dual" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".dual,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_decision_master";

CREATE TYPE "CP_Client"."_icp_decision_master" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_decision_master,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_downstream_audit";

CREATE TYPE "CP_Client"."_icp_downstream_audit" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_downstream_audit,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_dynamic_api";

CREATE TYPE "CP_Client"."_icp_dynamic_api" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_dynamic_api,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_dynamic_api_dtls";

CREATE TYPE "CP_Client"."_icp_dynamic_api_dtls" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_dynamic_api_dtls,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_error_codes";

CREATE TYPE "CP_Client"."_icp_error_codes" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_error_codes,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_erule_dtls";

CREATE TYPE "CP_Client"."_icp_erule_dtls" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_erule_dtls,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_erule_master";

CREATE TYPE "CP_Client"."_icp_erule_master" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_erule_master,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_exception_log";

CREATE TYPE "CP_Client"."_icp_exception_log" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_exception_log,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_field_master";

CREATE TYPE "CP_Client"."_icp_field_master" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_field_master,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_interfacing_system";

CREATE TYPE "CP_Client"."_icp_interfacing_system" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_interfacing_system,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_list_config";

CREATE TYPE "CP_Client"."_icp_list_config" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_list_config,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_list_config_dtls";

CREATE TYPE "CP_Client"."_icp_list_config_dtls" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_list_config_dtls,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_offering_decision";

CREATE TYPE "CP_Client"."_icp_offering_decision" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_offering_decision,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_offering_elimination";

CREATE TYPE "CP_Client"."_icp_offering_elimination" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_offering_elimination,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_offering_enrichment";

CREATE TYPE "CP_Client"."_icp_offering_enrichment" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_offering_enrichment,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_offering_fields";

CREATE TYPE "CP_Client"."_icp_offering_fields" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_offering_fields,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_offering_pref_impl";

CREATE TYPE "CP_Client"."_icp_offering_pref_impl" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_offering_pref_impl,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_parameter";

CREATE TYPE "CP_Client"."_icp_parameter" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_parameter,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_pref_details";

CREATE TYPE "CP_Client"."_icp_pref_details" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_pref_details,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_pref_hierarchy_impl";

CREATE TYPE "CP_Client"."_icp_pref_hierarchy_impl" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_pref_hierarchy_impl,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_pref_master";

CREATE TYPE "CP_Client"."_icp_pref_master" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_pref_master,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_processing_audit";

CREATE TYPE "CP_Client"."_icp_processing_audit" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_processing_audit,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_product_offering";

CREATE TYPE "CP_Client"."_icp_product_offering" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_product_offering,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_query_config";

CREATE TYPE "CP_Client"."_icp_query_config" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_query_config,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_query_config_input";

CREATE TYPE "CP_Client"."_icp_query_config_input" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_query_config_input,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_recommendation_audit";

CREATE TYPE "CP_Client"."_icp_recommendation_audit" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_recommendation_audit,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_upstream_audit";

CREATE TYPE "CP_Client"."_icp_upstream_audit" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_upstream_audit,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_vrule_dtls";

CREATE TYPE "CP_Client"."_icp_vrule_dtls" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_vrule_dtls,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_icp_vrule_master";

CREATE TYPE "CP_Client"."_icp_vrule_master" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".icp_vrule_master,
	DELIMITER = ',');

-- DROP TYPE "CP_Client"."_sp_transaction_master";

CREATE TYPE "CP_Client"."_sp_transaction_master" (
	INPUT = array_in,
	OUTPUT = array_out,
	RECEIVE = array_recv,
	SEND = array_send,
	ANALYZE = array_typanalyze,
	ALIGNMENT = 8,
	STORAGE = any,
	CATEGORY = A,
	ELEMENT = "CP_Client".sp_transaction_master,
	DELIMITER = ',');
