// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// Client:   14002 without popularity
// Internal: 14001 with popularity

export const environment = {
  production: true,
  // saveUrl: 'http://10.240.128.27:8051/ContextualUI/saveScreenData',
  saveUrl: 'http://10.240.128.27:14002/ContextualUI/saveScreenData',
  cpEngineUrl: 'http://10.240.128.27:14002/igtb/cp/api/getreco',
  fulfilmentDataUrl: 'http://10.240.128.27:14002/ContextualUI/getFulfilmentData', //dummy confirm with Shahinsha
  // fulfilmentDataUrl: 'http://172.29.134.79:8080/ContextualUI/getFulfilmentData',
  // fulfillDataUrl: 'http://172.29.130.135:8080/getFulfilData',
  validateRecoUrl: 'http://10.240.128.27:14002/igtb/cp/api/validatereco',
  referenceDataUrl: 'http://10.240.128.27:14002/ContextualUI/getReferenceData',
  // referenceDataUrl: 'http://172.29.134.79:8080/ContextualUI/getReferenceData',
  updateUrl: 'http://10.240.128.27:14002/ContextualUI/updateConfigData',
  // updateUrl: 'http://172.29.134.79:8080/ContextualUI/updateConfigData',
  editDataUrl: 'http://10.240.128.27:14002/ContextualUI/getSearchResult',
  // editDataUrl: 'http://172.29.134.79:8080/ContextualUI/getSearchResult'
  // accuracyUrl: 'http://10.240.128.27:5000/contextual/aiplugin/getaccuracy',
  saveRecoUrl: 'http://10.240.128.27:14002/igtb/cp/api/savereco'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
