// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// Client:   14002 without popularity
// Internal: 14001 with popularity

export const environment = {
  production: false,
  // saveUrl: 'http://10.240.128.27:8051/ContextualUI/saveScreenData',
  saveUrl: 'http://10.240.128.27:14002/ContextualUI/saveScreenData',
  // saveUrl: 'http://172.29.134.118:14002/ContextualUI/saveScreenData',
  cpEngineUrl: 'http://10.240.128.27:14002/igtb/cp/api/getreco',
  //cpEngineUrl: 'http://localhost:8080/cp/api/getreco',
  fulfilmentDataUrl: 'http://10.240.128.27:14002/ContextualUI/getFulfilmentData', //dummy confirm with Shahinsha
  // fulfilmentDataUrl: 'http://172.29.134.79:14002/ContextualUI/getFulfilmentData',
  // fulfillDataUrl: 'http://172.29.130.135:14002/getFulfilData',
  validateRecoUrl: 'http://10.240.128.27:14002/igtb/cp/api/validatereco',
  referenceDataUrl: 'http://10.240.128.27:14002/ContextualUI/getReferenceData',
  // referenceDataUrl: 'http://172.29.134.79:14002/ContextualUI/getReferenceData',
  updateUrl: 'http://10.240.128.27:14002/ContextualUI/updateConfigData',
  // updateUrl: 'http://172.29.134.79:14002/ContextualUI/updateConfigData',
  editDataUrl: 'http://10.240.128.27:14002/ContextualUI/getSearchResult',
  // editDataUrl: 'http://172.29.134.79:14002/ContextualUI/getSearchResult'
  // accuracyUrl: 'http://10.240.128.27:5000/contextual/aiplugin/getaccuracy',
  saveRecoUrl: 'http://10.240.128.27:14002/igtb/cp/api/savereco',
  //cashFlowurl : 'http://localhost:3000/response'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
