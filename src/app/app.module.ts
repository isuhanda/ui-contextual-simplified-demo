import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { NgSelectModule } from '@ng-select/ng-select';
import { ClarityModule } from "@clr/angular";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ContextualPaymentsLoginComponent } from './contextual-payments-login/contextual-payments-login.component';
import { ContextualPaymentsHeaderComponent } from './contextual-payments-header/contextual-payments-header.component';
import { ContextualPaymentsInputScreenComponent } from './contextual-payments-input-screen/contextual-payments-input-screen.component';
import { RouterModule, Routes } from '@angular/router';
import { ContextualPaymentsFulfilmentComponent } from './contextual-payments-fulfilment/contextual-payments-fulfilment.component';
import { HorizontalTimelineComponent } from './horizontal-timeline/horizontal-timeline.component';
//rimport { CheckerComponentComponent } from './checker-component/checker-component.component';
import { ContextualPaymentsFulfilmentListingComponent } from './contextual-payments-fulfilment-listing/contextual-payments-fulfilment-listing.component';
import { ContextualPaymentsValidFulfilmentsComponent } from './contextual-payments-valid-fulfilments/contextual-payments-valid-fulfilments.component';
import {FulfilmentApiService} from './services/fulfilmentapi.component';
import {SelectObject} from './selectedObject';
import { DateFormatPipe } from './DateFormatPipe';

import { ChartModule } from '@syncfusion/ej2-ng-charts';
import { CategoryService, ColumnSeriesService, LineSeriesService} from '@syncfusion/ej2-ng-charts';
import { CheckerComponentComponent } from './checker-component/checker-component.component';
import { DatePipe } from '@angular/common';
import { ContextualPaymentCashFlowComponent } from './contextual-payment-cash-flow/contextual-payment-cash-flow.component';
@NgModule({
  declarations: [
    AppComponent,
    ContextualPaymentsLoginComponent,
    ContextualPaymentsHeaderComponent,
    ContextualPaymentsInputScreenComponent,
    ContextualPaymentsFulfilmentComponent,
    HorizontalTimelineComponent,
    DateFormatPipe,
    CheckerComponentComponent,
    ContextualPaymentsFulfilmentListingComponent,
    ContextualPaymentsValidFulfilmentsComponent,
    ContextualPaymentCashFlowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgSelectModule,
    ClarityModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    ChartModule,
    HttpClientModule
  ],
  providers: [FulfilmentApiService,SelectObject,DatePipe,CategoryService,LineSeriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
