import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckerComponentComponent } from './checker-component.component';

describe('CheckerComponentComponent', () => {
  let component: CheckerComponentComponent;
  let fixture: ComponentFixture<CheckerComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckerComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckerComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
