import { Component, OnInit, Input } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router'
import { element } from 'protractor';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';
// import { ContextPaymentsInputScreenComponent } from '';


@Component({
  selector: 'app-contextual-payments-fulfilment',
  templateUrl: './contextual-payments-fulfilment.component.html',
  styleUrls: ['./contextual-payments-fulfilment.component.css']
})
export class ContextualPaymentsFulfilmentComponent implements OnInit {
  public responseObj: any = {
    debtorAccount: String,
    customerSector: String,
    debtorAccountBranch: String,
    userId: String,
    initiationTime: String,
    instructedCurrency: String,
    initiationDate: String,
    customerSegment: String,
    offering: [],
    customerId: String,
    instructedAmount: String,
    debtorAccountCurrency: String,
    customerCategory: String,
    creditorBankCountry: String
  };
  public fulfilmentfieldsArray: [] = [];
  public currentNavigation: any;
  public loginUserID: string = "";
  public inputFormData: string;
  public inputFormDataObject: Object;
  public selectedFulfilment: Object;
  public contextualRefId: String;
  public notionalRateData: Object;
  public txnAmount: String;
  public txnCurrency: String;
  public scoreCost: number = 0;
  public scoreCostDisplay: String = "";
  public scoreTime: number = 0;
  public scoreTimeDisplay: String = "";
  public offeringRecommended: String;

  /*
  public offeringFieldMapping = {
    'PRD_OFRNG_ETRF_001' : ['Mobile Number','Email ID'],
    'PRD_OFRNG_EFT_001': ['Creditor Bank Name','Creditor Bank Type','Creditor Bank Routing No'],
    'PRD_OFRNG_ACH_001': ['Creditor Bank Routing No','Creditor Account Type'],
    'PRD_OFRNG_WIRE_001': ['Creditor ABA Routing No','Creditor Bank Address','Creditor Bank Name','Creditor Name'],
    'PRD_OFRNG_GMT_001': ['SWIFT Code','Creditor Name','Creditor Address']
  }
  */

  public offeringFieldMapping = {
    'PRD_OFRNG_ETRF_001': ['Phone', 'Email ID', 'Remittance Info'],
    'PRD_OFRNG_EFT_001': ['Creditor Bank Name', 'Creditor Bank Routing No', 'Creditor Account No'],
    'PRD_OFRNG_ACH_001': ['Creditor Account Type', 'Creditor Bank Routing No', 'Creditor Account No', 'Remittance Info'],
    'PRD_OFRNG_WIRE_001': ['Creditor Name', 'Creditor Address', 'Creditor Bank Name', 'Creditor Bank Address', 'Creditor BIC Routing No', 'Details of Charges', 'Remittance Info'],
    'PRD_OFRNG_GMT_001': ['SWIFT Code', 'Creditor Name', 'Creditor Address']
  };

  public contactNames = [
    {
      'name' : 'TBC001',
      'accNo' : '97388233998',
      'country' : 'USA'
    },
    {
      'name' : 'TBC002',
      'accNo' : '97388233999',
      'country' : 'CA'
    },
    {
      'name' : 'TBC003',
      'accNo' : '97388233899',
      'country' : 'CA'
    },
    {
      'name' : 'TBC004',
      'accNo' : '97388233990',
      'country' : 'CA'
    }
  ];
  public payFromAccs = [
    {
      'name': 'PayFromAcc1',
      'accNo': '987654321',
      'currency': 'CAD',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc2',
      'accNo': '975319861',
      'currency': 'INR',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc3',
      'accNo': '975319451',
      'currency': 'AUD',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc4',
      'accNo': '975356861',
      'currency': 'CAD',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc5',
      'accNo': '975387861',
      'currency': 'USD',
      'branch': 'Goregaon'
    }
  ]

  public currencyList = ['CAD','INR','USD','AUD'];
  public countryList = [
    {
      'name' : 'India',
      'value': 'IND'
    },
    {
      'name' : 'United States of America',
      'value': 'US'
    },
    {
      'name' : 'Canada',
      'value': 'CA'
    },
    {
      'name' : 'Australia',
      'value': 'AUS'
    },
    {
      'name' : 'South Africa',
      'value': 'SA'
    },
    {
      'name' : 'Sweden',
      'value': 'SW'
    },
  ];

  constructor(
    private httpService: HttpClient,
    private router: Router, private route: ActivatedRoute,public datepipe: DatePipe
   ) {
     console.log("Constructor of class contextual-payments-fulfilment ts");
    this.currentNavigation = this.router.getCurrentNavigation();
       // super(router, preferenceService, paymentService, paymentEntitlementService, 'cibc-payment-summary-in-workflow');
      // super(preferenceService, 'contextual-payments-send-money');
       //this.componentId = '';
       console.log('Constructor of CP fulfilment called.');
       this.route.queryParams.subscribe(params => {
         console.log("Ankur");
         console.log("params is: ");
         console.log(params);
         this.loginUserID = params["loginObject"]
         this.inputFormData = params["inputFormData"];
         this.contextualRefId = params["contextualRefId"];
         this.notionalRateData = JSON.parse(params["notionalRateData"]);
         this.offeringRecommended = params["offeringRecommended"];
         this.selectedFulfilment = JSON.parse(params["selectedFulfilment"]);
         console.log("InputFormData: "+this.inputFormData);
         this.inputFormDataObject = JSON.parse(this.inputFormData);
         this.txnAmount = this.inputFormDataObject["payload"]["instructedAmount"];
         this.txnCurrency = this.inputFormDataObject["payload"]["instructedCurrency"];
        this.setScoreValDisplay(this.selectedFulfilment);
         console.log("The inputFormDataObject Form is: ");
         console.log(this.inputFormDataObject);
         console.log("ContextualRefID: ");
         console.log(this.contextualRefId);
         console.log("notionalRateData: ");
         console.log(this.notionalRateData);
         console.log("Offering Recommended: ");
         console.log(this.offeringRecommended);
         console.log("Selected Fulfilment");
         console.log(this.selectedFulfilment);
         console.log(this.inputFormDataObject["amount"]);
         console.log("The selected Fulfilment is: ");
         console.log(this.selectedFulfilment);
         console.log("Madiyan");
       })
    }
    public cbxInputForm: FormGroup = new FormGroup({
      contactName : new FormControl(''),
      country : new FormControl(''),
      debtorAccount : new FormControl(''),
      amount : new FormControl(''),
      currency : new FormControl(''),
      receiveOnDate : new FormControl('')
    });
    public cbxFulfilmentForm: FormGroup = new FormGroup({
      contactName : new FormControl({value: null, disabled: true}),
      country : new FormControl({value: null, disabled: true}),
      debtorAccount : new FormControl({value: null, disabled: true}),
      amount : new FormControl({value: null, disabled: true}),
      currency : new FormControl({value: null, disabled: true}),
      receiveOnDate : new FormControl({value: null, disabled: true}),
      fulfilmentfield1 : new FormControl(null),
      fulfilmentfield2 : new FormControl(null),
      fulfilmentfield3 : new FormControl(null),
      fulfilmentfield4 : new FormControl(null),
      fulfilmentfield5 : new FormControl(null),
      fulfilmentfield6 : new FormControl(null),
      fulfilmentfield7 : new FormControl(null),
      fulfilmentfield8 : new FormControl(null)
    });
    
    public payFromValues: string[] = [];
    public corporateKeys: string[] = [];
    public factorscore = '';
    public entlFunction = 'add';
    public listOfValues: any[]= [];
    public fulfill: any[]= [];
    public decisionParams: any[]= [];
    public serviceKey: any;
    public noAccountFound = false;
    public accounts: any[] = [];
    public contactName: any;
    public country: String;
    public debtorAccount: String;
    public amount: String;
    public currency: String;
    public receiveOnDate: String;
    public debitAmount: String;
    public debitAmountCcy: String;
    public notionalRate: String;
    public showCrossCurrencyDiv: boolean = false;
    public showResults = false;
    public cpEngineSaveUrl:string = environment.saveUrl;
    public dataSaveFailure = false;
    public dataSaveSuccess = false;
    public dataSaveTechError = false;
 
    ngOnInit() {
      console.log("The selected fulfilment received is: ");
      console.log(this.selectedFulfilment);
      console.log("Fulfilment Oninit called");
      this.getFulfilmentFields();
      console.log("The fulfilment fields array populated is: ");
      console.log(this.fulfilmentfieldsArray);
      console.log("Input FormObject: ");
      console.log(this.inputFormDataObject["payload"]["creditorName"]);
      this.contactName = this.inputFormDataObject["payload"]["creditorName"];
      console.log("ContactName value is now set");
      console.log("Date set....",this.inputFormDataObject["payload"]["valueDate"]);
      
      this.country = this.fetchCountryFromCode(this.inputFormDataObject["payload"]["creditorBankCountry"]);
      this.debtorAccount = this.inputFormDataObject["payload"]["customerId"];
      this.amount = this.inputFormDataObject["payload"]["instructedAmount"];
      this.currency = this.inputFormDataObject["payload"]["instructedCurrency"];
      //this.receiveOnDate = this.inputFormDataObject["payload"]["valueDate"];
      
      console.log("the get Date",this.notionalRateData["CurrentDate"]);
      
      this.receiveOnDate = this.datepipe.transform(this.notionalRateData["CurrentDate"], 'MM/dd/yyyy');
      console.log("aaaa",this.datepipe.transform(this.notionalRateData["CurrentDate"], 'MM/dd/yyyy'));
      
      this.debitAmount = this.notionalRateData["debitAmount"];
      this.debitAmountCcy = this.notionalRateData["debitAmountCcy"];
      this.notionalRate = this.notionalRateData["notionalRate"];
      console.log("country: "+this.country);
      console.log("debtorAccount: "+this.debtorAccount);
      console.log("amount: "+this.amount);
      console.log("currency: "+this.currency);
      console.log("receiveOnDate: "+this.receiveOnDate);
      console.log("debitAmount: "+this.debitAmount);
      console.log("notionalRate: "+this.notionalRate);

      if(this.debitAmountCcy !== this.currency) {
        this.showCrossCurrencyDiv = true;
      }
      // console.log(this.inputFormDataObject["contactName"].name);
      // this.contactName = this.inputFormDataObject["contactName"].name;
      // var dummy = this.inputFormDataObject["contactName"].name;
      // console.log("Dummy Var: "+dummy);
      // console.log(this.contactName);
      // this.contactName = 

     }

     public fetchCountryFromCode(code: String): String 
     {
       console.log("The country code received is: ");
       console.log(code);
       var countryName: String = "";
       this.countryList.forEach(element => {
         if(element.value === code) {
           countryName = element.name;
         }
       })
       return countryName;
     }

     public getFulfilmentFields(): void 
     {
        var fulfilmentCode = this.selectedFulfilment["offeringCode"];
        console.log("The offering code selected is: ");
        console.log(fulfilmentCode);
        console.log(this.offeringFieldMapping[fulfilmentCode]);
        this.fulfilmentfieldsArray = this.offeringFieldMapping[fulfilmentCode];
     }

     public submitForm(): void {
      console.log("The input formobject is: ")
      console.log(this.inputFormDataObject);
      console.log("The fulfilment form is: ")
      console.log(this.cbxFulfilmentForm);
      console.log("selectedFulfilment: ");
      console.log(this.selectedFulfilment);
      console.log("ContextualRefID");
      console.log(this.contextualRefId);
      console.log("notional rate data object is: ");
      console.log(this.notionalRateData);
      var data: {} = {
        "SP_TRANSACTION_MASTER": {
        'id_channel_reqid': this.inputFormDataObject["context"]["channelRequestId"],
        'id_contextual_refid': this.contextualRefId,
        'in_dr_ac': this.inputFormDataObject["payload"]["debtorAccount"],
        'in_dr_ac_ccy': this.inputFormDataObject["payload"]["debtorAccountCurrency"],
        'in_cr_agt_ctry': this.inputFormDataObject["payload"]["creditorBankCountry"],
        //'in_val_dt': this.inputFormDataObject["payload"]["valueDate"],
        'in_val_dt': this.datepipe.transform(this.notionalRateData["CurrentDate"], 'MM/dd/yyyy'),
        'in_inst_amt':  +this.notionalRateData["instructedAmount"],
        'in_inst_amt_ccy': this.notionalRateData["instructedAmountCcy"],
        'in_debt_amt': +this.notionalRateData["debitAmount"],
        'in_debt_amt_ccy': this.notionalRateData["debitAmountCcy"],
        'fl_ntnl_rate': this.notionalRateData["notionalRate"],
        'in_cust_id': this.inputFormDataObject["payload"]["customerId"],
        'offering_code_recommended': this.offeringRecommended ? this.offeringRecommended : 'PRD_OFRNG_GMT_001',//this.offeringRecommended
        'offering_code_selected': this.selectedFulfilment["offeringCode"],
        'decision_score_cost': +this.scoreCost,
        'decision_score_time': +this.scoreTime,
        'decision_score_display_cost': this.scoreCostDisplay,
        'decision_score_display_time': this.scoreTimeDisplay,
        'ful_field1': this.cbxFulfilmentForm.value.fulfilmentfield1,
        'ful_field2': this.cbxFulfilmentForm.value.fulfilmentfield2,
        'ful_field3': this.cbxFulfilmentForm.value.fulfilmentfield3,
        'ful_field4': this.cbxFulfilmentForm.value.fulfilmentfield4,
        'ful_field5': this.cbxFulfilmentForm.value.fulfilmentfield5,
        'ful_field6': this.cbxFulfilmentForm.value.fulfilmentfield6,
        'ful_field7': this.cbxFulfilmentForm.value.fulfilmentfield7,
        'ful_field8': this.cbxFulfilmentForm.value.fulfilmentfield8,
        'maker_id': this.loginUserID,
        'MAKER_DATE': null,
        'status': 'I'
        }
      };
 
      console.log('Printing the record being inserted.');
      console.log(data);
      //const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'X-Request-Id': '21f712dc-43a5-4b0b-b42a-77394e28c00b', 'ChannelId': 'CBX' });
      const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'X-Request-Id': '426e24d1-d63f-4504-b38c-b4bb76612218', 'ChannelId': 'CBX' });
      console.log('this.cpEngineSaveURL: ' + this.cpEngineSaveUrl);
     // console.log(ENVIRONMENT.intellect.cpEngineUrl);
      this.httpService.post(this.cpEngineSaveUrl,
      data, {headers: myHeaders}).subscribe(data => {
          console.log("Printing the response from the save url: ");
          console.log(data);
          if(data === 1)
          {
            console.log("The response is 1");
            this.dataSaveSuccess = true;
          }else{
            console.log("The response is not 1");
            this.dataSaveFailure = true;
          }
        }, error => {
          console.log('DB Save Error');
          console.log(error);
          this.dataSaveTechError = true;

        });
    }
    // alert("Ankur Madiyan");
    // this.router.navigate(['/login']);
    public setScoreValDisplay(selectedFulfilment: Object): void {
      console.log("The selected Fulfilment while score cost cal is: ");
      console.log(selectedFulfilment);
      selectedFulfilment["decisionParams"].forEach(element => {
        console.log("Decision code")
        console.log(element.decisionCode);
        if(element.decisionCode === 'COST') {
          this.scoreCost = element.score;
          this.scoreCostDisplay = element.scoreDisplay;
        }
        if(element.decisionCode === 'TIME_VD') {
          console.log("Setting the scoreTime");
          this.scoreTime = element.score;
          this.scoreTimeDisplay = element.scoreDisplay;
        }
        console.log("Displaying the scoreTime");
        console.log(this.scoreCost);
        console.log(this.scoreCostDisplay);
        console.log(this.scoreTime);
        console.log(this.scoreTimeDisplay);
      });
    }

    public routeToLogin(): void {
      this.dataSaveSuccess = false;
      this.dataSaveFailure = false;
      this.dataSaveTechError = false;
      this.router.navigate(['/login']);
    }


    public cancelMe(): void {
      window.history.back();
      // this.router.navigate(['/input']);

    }

}
