import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualPaymentsFulfilmentComponent } from './contextual-payments-fulfilment.component';

describe('ContextualPaymentsFulfilmentComponent', () => {
  let component: ContextualPaymentsFulfilmentComponent;
  let fixture: ComponentFixture<ContextualPaymentsFulfilmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextualPaymentsFulfilmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualPaymentsFulfilmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
