import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { element } from 'protractor';

@Component({
  selector: 'app-contextual-payments-login',
  templateUrl: './contextual-payments-login.component.html',
  styleUrls: ['./contextual-payments-login.component.css']
})
export class ContextualPaymentsLoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  selectedUser: string = "";
  loginUser: any = ['James Dawson','Michelle Morgan','Sydney Carter','Robert Langdon','Andrew Roberts'];
  passwordEntered: string;
  isUserNameValid: boolean = false;
  isPasswordValid: boolean = false;
  showErrorMessage: boolean = false;
  userRole: string = "";
  customer_name = "";
  category_name = "";
  loginUserPasswordArray: any = [
    {
      username: "James Dawson",
      password: "asd123",
      role: "MAKER",
      customer_id: "CEB09345",
      //customer_id: "James123",
      customer_name: "James Dawson",
      category_id: "Small Businesses",
      category_name: "Small Businesses"
    },
    {
      username: "Michelle Morgan",
      password: "asd123",
      role: "MAKER",
      customer_id: "CEB08971",
      //customer_id: "Michelle123",
      customer_name: "Michelle Morgan",
      category_id: "Large Corporates",
      category_name: "Large Corporates"
    },
    {
      username: "Sydney Carter",
      password: "asd123",
      role: "MAKER",
      //customer_id: "Sydney123",
      customer_id: "CEB07894",
      customer_name: "Sydney Carter",
      category_id: "Small Businesses",
      category_name: "Small Businesses"
    },
    {
      username: "Robert Langdon",
      password: "asd123",
      role: "CHECKER",
      customer_id: "CEB23478",
      //customer_id: "Robert123",
      customer_name: "Robert Langdon",
      category_id: "Small Businesses",
      category_name: "Small Businesses"
    },
    {
      username: "Andrew Roberts",
      password: "asd123",
      role: "CHECKER",
      customer_id: "CEB30826",
      //customer_id: "Andrew123", // Reverted by Gaurav: DB had CEB style Users
      customer_name: "Andrew Roberts",
      category_id: "Large Corporates",
      category_name: "Large Corporates"
    },
  ];

  getUserRole(): void {
    this.showErrorMessage = false;
    this.userRole="";
    console.log("Fetching User Role");
    this.loginUserPasswordArray.forEach(element => {
      console.log(element.username);
      console.log(element.password);
      if(element.username === this.selectedUser) {
        this.userRole = element.role;
        this.customer_name = element.customer_name;
        this.category_name = element.category_name;
        console.log("The user role assigned is: "+this.userRole);
        this.isUserNameValid = true;
      }
    });
    if(this.isUserNameValid === false) {
      this.showErrorMessage = true;
    }
  }

  public loginUserObject: Object;

  navigationExtras: NavigationExtras = {
    queryParams: {
      "loginObject": String
    }
  }

  validatePassword(): void {
    console.log("The username selected is: ");
    console.log(this.selectedUser);
    console.log("The password entered is: ");
    console.log(this.passwordEntered);
    var userPassword: string = "";
    console.log("Printing all users");
    this.loginUserPasswordArray.forEach(element => {
      console.log(element.username);
      console.log(element.password);
      if(element.username === this.selectedUser) {
        console.log("User Found");
        this.isUserNameValid = true;
        if(this.passwordEntered === element.password) {
          console.log("Password Entered matched");
          this.isPasswordValid = true;
          this.userRole = element.role;
          console.log("Role: "+this.userRole);
        }
      }
    });

    if(this.isUserNameValid && this.isPasswordValid) {
      console.log("Password and User Valid")
      var userRole: string = "";
      this.loginUserPasswordArray.forEach(element  => {
        if(element.username === this.selectedUser) {
          this.loginUserObject = element;
          sessionStorage.setItem('loggedInUser', JSON.stringify(this.loginUserObject));
          if(element.role === 'CHECKER') {
            this.router.navigate(['/checker-flow'],this.navigationExtras);
          }else{
            this.loginUserObject = element;
            this.navigationExtras.queryParams.loginObject = JSON.stringify(this.loginUserObject);
            sessionStorage.setItem('loggedInUser', JSON.stringify(this.loginUserObject));
            //this.router.navigate(['/input'],this.navigationExtras);
            this.router.navigate(['/checker-flow'],this.navigationExtras);
          }
        }
      });
      this.showErrorMessage = false;
    }else{
      this.showErrorMessage = true;
      console.log("Error message");
    }
  }
};
