import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualPaymentsLoginComponent } from './contextual-payments-login.component';

describe('ContextualPaymentsLoginComponent', () => {
  let component: ContextualPaymentsLoginComponent;
  let fixture: ComponentFixture<ContextualPaymentsLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextualPaymentsLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualPaymentsLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
