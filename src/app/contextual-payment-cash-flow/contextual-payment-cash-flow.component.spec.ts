import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualPaymentCashFlowComponent } from './contextual-payment-cash-flow.component';

describe('ContextualPaymentCashFlowComponent', () => {
  let component: ContextualPaymentCashFlowComponent;
  let fixture: ComponentFixture<ContextualPaymentCashFlowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextualPaymentCashFlowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualPaymentCashFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
