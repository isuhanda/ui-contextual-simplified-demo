import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable()
export class FulfilmentApiService{

  url = environment.fulfilmentDataUrl;
//  cashFlowurl = environment.cashFlowurl;

 constructor(private httpClient: HttpClient) {
    console.log('FulfilmentApiService......');
  }

// cashFlowUrl(){
//   console.log('cashFlowURL' +this.cashFlowUrl);
//  //return this.httpClient.get( 'get' + this.cashFlowUrl).subscribe (result => result);
// }
getFulfilments(qurycode: string) {
    console.log('getValidationRulesData service...........'+this.url);
    this.url = environment.fulfilmentDataUrl;
    let body = {
      queryCode: qurycode
    };
    return this.httpClient
      .post(environment.fulfilmentDataUrl, body, {
        headers: new HttpHeaders().set('Content-Type', "application/json")
      })
      .pipe(map((response: any) => response));
  }


getValidFulfilments(data:any) : Observable<any>{
  console.log("The data being sent for valid fulfilments");
  console.log(data);
       let myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });

        return this.httpClient.post(environment.validateRecoUrl,data,{headers : myHeaders});

     }



 getSPFullfilments(data:any) : Observable<any>{
       let myHeaders = new HttpHeaders({ 'Content-Type': 'application/json'});

        return this.httpClient.post(environment.referenceDataUrl,data,{headers : myHeaders});
        //http://10.196.11.52:8051/getReferenceData
     }

  update(dataMap: {}, deleteMap: {}) {
    const body = {
      tbldata: dataMap,
      deldata: deleteMap
    };
    return this.httpClient.post<any>(environment.updateUrl, body);
  }


 getSearchObject(primaryKey: string, queryCode: string) : Observable<any>{
       let myHeaders = new HttpHeaders({ 'Content-Type': 'application/json'});
    const data = {
      queryCode: queryCode,
      primaryKey: primaryKey
    };
        return this.httpClient.post(environment.editDataUrl,data,{headers : myHeaders});
        //http://10.196.11.52:8051/getReferenceData
  }
  // getSearchObject(primaryKey: string, queryCode: string) {
  //   console.log("getValidationRulesData service...........");
  //   this.url = "http://10.196.11.52:8051";
  //   console.log("primary KEY:::",primaryKey);
  //   console.log("queryCOde:",queryCode);
  //   const req = {
  //     queryCode: queryCode,
  //     primaryKey: primaryKey
  //   };
  //   return this.httpClient
  //     .post(this.url + "/getEditData", req, {
  //       headers: new HttpHeaders().set("Content-Type", "application/json",)
  //     })
  //     .pipe(map((response: any) => response));
  // }
}
