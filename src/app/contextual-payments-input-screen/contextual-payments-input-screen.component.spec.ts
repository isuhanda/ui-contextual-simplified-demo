import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualPaymentsInputScreenComponent } from './contextual-payments-input-screen.component';

describe('ContextualPaymentsInputScreenComponent', () => {
  let component: ContextualPaymentsInputScreenComponent;
  let fixture: ComponentFixture<ContextualPaymentsInputScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextualPaymentsInputScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualPaymentsInputScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
