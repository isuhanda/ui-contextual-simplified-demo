import { Component, OnInit, EventEmitter, Output, PipeTransform, Pipe } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FormControl, FormGroup} from '@angular/forms';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router'
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';
import { v4 as uuid } from 'uuid';
import { decode } from 'punycode';

@Component({
  selector: 'app-contextual-payments-input-screen',
  templateUrl: './contextual-payments-input-screen.component.html',
  styleUrls: ['./contextual-payments-input-screen.component.css']
})
export class ContextualPaymentsInputScreenComponent implements OnInit {

  public responseObj: any = {
    context: {
      channelRequestId: String,
      contextualReferenceId: String
    },
    payload: {
      debtorAccount: String,
      customerSector: String,
      debtorAccountBranch: String,
      userId: String,
      initiationTime: String,
      instructedCurrency: String,
      initiationDate: String,
      customerSegment: String,
      offering: [],
      customerId: String,
      instructedAmount: String,
      debtorAccountCurrency: String,
      customerCategory: String,
      creditorBankCountry: String,
      eligible: String
    }
  };

  constructor(
    private httpService: HttpClient,
    private router: Router, private route: ActivatedRoute
   ) {
       // super(router, preferenceService, paymentService, paymentEntitlementService, 'cibc-payment-summary-in-workflow');
      // super(preferenceService, 'contextual-payments-send-money');
      this.cbxProfileForm =  new FormGroup({

        contactName : new FormControl(null),
        country : new FormControl(null),
        debtorAccount : new FormControl(null),
        amount : new FormControl(null),
        currency : new FormControl(null),
        receiveOnDate : new FormControl(null)
        });
      console.log('Constructor of CP fulfilment called.');
      this.route.queryParams.subscribe(params => {
         console.log("Ankur");
         console.log("Params received are: ");
         console.log(params);
         this.loginObject = JSON.parse(params["initObject"]);
         console.log("The login object is: ");
         console.log(this.loginObject);
        })
      console.log('Constructor of side drawer called.');

    }
    public loginObject: Object;
    public cbxProfileForm: FormGroup;
    @Output() formEmitter = new EventEmitter<any>();
    public payFromValues: string[] = [];
    public corporateKeys: string[] = [];
    public factorscore = '';
    public entlFunction = 'add';
    public listOfValues: any[] = [];
    public fulfill: any[] = [];
    public accuracy: any;
    public decisionParams: any[] = [];
    public serviceKey: any;
    public noAccountFound = false;
    public accounts: any[] = [];
    public showResults = false;
    public showCrossCurrencyDiv = false;
    public notionalRate: number = 1;
    public debitAmount: number = 0;
    public selectedFulfilment: any;
    public options: any;
    public isEmpty:boolean = false;
    public knowMore:boolean =false;

    public isClick:boolean=false;
    public message:any;
    public ntRecoMes:any;
    public invertedStarValue: number[][];
    public eligibleStarValues: number[][];
    // public popularity:'';
    public speed: '';
    public cost: '';
    public fromCurrencyType: '';

    public contactNames = [
      {
        //'name' : 'TBC001',
        'name' :'Simon Jones',
        'accNo' : '97388233998',
        'country' : 'USA'
      },
      {
        //'name' : 'TBC002',
        'name' : 'Stephen Smith',
        'accNo' : '97388233999',
    	  'country' : 'CA'
      },
      {
        //'name' : 'TBC003',
        'name' : ' Marnus Warner',
        'accNo' : '97388233899',
        'country' : 'CA'
      },
      {
        //'name' : 'TBC004',
        'name' : 'Kenneth Miles',
        'accNo' : '97388233990',
        'country' : 'CA'
      }
    ];
    public payFromAccs = [
      {
        'name': 'PayFromAcc1',
        'accNo': '987654321',
        'currency': 'CAD',
        'branch': 'Goregaon',
        'value': '#0000ff'
      },
      {
        'name': 'PayFromAcc2',
        'accNo': '975319861',
        'currency': 'INR',
        'branch': 'Goregaon'
      },
      {
        'name': 'PayFromAcc3',
        'accNo': '975319451',
        'currency': 'AUD',
        'branch': 'Goregaon'
      },
      {
        'name': 'PayFromAcc4',
        'accNo': '975356861',
        'currency': 'CAD',
        'branch': 'Goregaon'
      },
      {
        'name': 'PayFromAcc5',
        'accNo': '975387861',
        'currency': 'USD',
        'branch': 'Goregaon'
      }
    ]

    public currencyList = ['CAD','INR','USD','AUD'];
    public countryList = [
      {
        'name' : 'India',
        'value': 'IND'
      },
      {
        'name' : 'United States of America',
        'value': 'US'
      },
      {
        'name' : 'Canada',
        'value': 'CA'
      },
      {
        'name' : 'Australia',
        'value': 'AUS'
      },
      {
        'name' : 'South Africa',
        'value': 'SA'
      },
      {
        'name' : 'Sweden',
        'value': 'SW'
      },
    ]
    // public cpEngineURL:string = "http://10.196.11.52:8071/igtb/cp/api/getreco";
    public cpEngineURL:string = environment.cpEngineUrl;
    // public accuracyUrl: string = environment.accuracyUrl;

    // public saveRecoURL:string = "http://10.196.11.33:14001/igtb/cp/api/savereco";
    public saveRecoURL:string = environment.saveRecoUrl;
    ngOnInit() {
     }

     EditForm
     public EditForm1(): void {
      this.isClick=false;
     }
     public submitForm1(): void {
      //  console.log("Inside submit");
      // let myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
      // console.log("Inside headers" +myHeaders);
      //  this.httpService.get('http://10.196.11.33:5000/contextual/aiplugin/getaccuracy').subscribe(data =>{
      //   console.log('Accuracy yaha hai..... ',data,{headers : myHeaders})
      // });
      const myHeaders = new HttpHeaders({
      'Content-Type': 'application/json', 'AccIess-Control-Allow-Origin': '*',
       'X-Request-Id': '21f712dc-43a5-4b0b-b42a-77394e28c00b', ChannelId: 'CBX' });
     // console.log('The accuracyUrl from environment file is: ');
      //console.log('this.accuracyUrl: ' + this.accuracyUrl);
      // this.httpService.get(this.accuracyUrl).subscribe(data =>{
      // console.log('Accuracy yaha hai..... ',data)
      // this.accuracy = data['accuracyDisplay'];
      // });

      console.log("Account no",this.cbxProfileForm.value.debtorAccount.name);

     // this.isClick=true;
      let map = new Map<String, String>();
      map.set("PayFromAcc1","20000 | 40000");
      map.set("PayFromAcc2","21000 | 71000");
      map.set("PayFromAcc3","22000 | 72000");
      map.set("PayFromAcc4","23000 | 83000");
      map.set("PayFromAcc5","24000 | 64000");


      var amt =map.get(this.cbxProfileForm.value.debtorAccount.name);
      const arr =amt.split("|");
      let x =parseFloat(arr[0]);
      let y=parseFloat(arr[1]);
      let z=parseFloat(this.cbxProfileForm.value.amount);
      this.message="";

      if(z<=x)
{
  console.log("low");
  this.submitForm();
}else if((z>=x) && (z<=y))
{
  console.log("upper");
  this.message="The Debit Account "+ this.cbxProfileForm.value.contactName.accNo + " selected by you for the transcation has an upcoming payment which may result in low balance? "+
  "Are you sure you want to continue?";
  this.isClick=true;
}else if(z>=y)
{
  this.message="The Debit Account " + this.cbxProfileForm.value.contactName.accNo  +" selected by you for the transaction has insufficient funds."+
  "Are you sure you want to continue?";
  console.log("max");
  this.isClick=true;
}

     }

     requestObj: any = {};
     headerReqId: any = {};
     public submitForm(): void {
      this.isClick=false;
      console.log("The form submitted is: ");
      console.log(this.cbxProfileForm);
      this.showResults = true;
      var randomNumber = Math.floor((Math.random()*(999999999-100000000))+100000000);//300346999
      console.log("The random number generated is: "+randomNumber);
      console.log("The login Object is: ");
      console.log(this.loginObject);
      console.log("category_id: ");
      console.log(this.loginObject["category_id"]);
      console.log("customer_id: ");
      console.log(this.loginObject["customer_id"]);
      this.requestObj = {
        'context': {
          'action': 'getreco',
          'channelRequestId': randomNumber.toString(),
          'channelId': 'CBX',
          'userId': 'MKR1'
        },
        'payload': {
          'debtorAccount': this.cbxProfileForm.value.debtorAccount.accNo,
          'debtorAccountBranch': this.cbxProfileForm.value.debtorAccount.branch,
          'debtorAccountCurrency': this.cbxProfileForm.value.debtorAccount.currency,
          'creditorBankCountry': this.cbxProfileForm.value.country.value,
          'creditorName': this.cbxProfileForm.value.contactName.name,
          'customerSegment': 'Business Banking',
          'customerCategory': this.loginObject["category_id"],//'Smart Banking'
          'customerSector': 'AWSE',
          'instructedAmount': this.cbxProfileForm.value.amount,
          'instructedCurrency': this.cbxProfileForm.value.currency,
          'initiationTime': this.getTime(),
          'customerId': this.cbxProfileForm.value.debtorAccount.name,
          'userId': this.loginObject["customer_id"], // USER_KIRAN
          'initiationDate': this.getDate(),
          'valueDate': this.cbxProfileForm.value.receiveOnDate,
          'contact':this.cbxProfileForm.value.contactName.name
        }
      };
      console.log("Contact Name",this.cbxProfileForm.value.contactName.name);

      console.log('Printing payload.');
      console.log(this.requestObj);
      console.log('Going to hit the server.');
      this.headerReqId = uuid();
      console.log("headerReqId for getreco");
      console.log(this.headerReqId);
      //const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'AccIess-Control-Allow-Origin': '*', 'X-Request-Id': '21f712dc-43a5-4b0b-b42a-77394e28c00b', 'ChannelId': 'CBX' });
      const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'AccIess-Control-Allow-Origin': '*', 'X-Request-Id': this.headerReqId, 'ChannelId': 'CBX' });
      console.log("myheader",myHeaders);
      console.log("The url from environment file is: ");
      console.log('this.cpEngineURL: ' + this.cpEngineURL);
     // console.log(ENVIRONMENT.intellect.cpEngineUrl);
      this.httpService.post(this.cpEngineURL,
      this.requestObj, {headers: myHeaders}).subscribe(data => {
          // alert("Calling Botton")
          console.log("Before getreco data: ");
          console.log(data);
          console.log('Transaction done successfully!');
          console.log('CP Call Success');
          console.log("After getreco data: ");
          console.log(data);
          this.responseObj = data;
          this.listOfValues = this.responseObj.payload.offering;
          this.listOfValues = this.listOfValues.sort(function(a,b){
            // return a.eli > b.recommendationOrder ? 0 : -1;
            if (a.eligible == "Y"){
              return -1;
            }
          });
          console.log("The list of values after sorting is: ");
          console.log(this.listOfValues);
          this.setRadioButton(this.listOfValues);
          if(this.listOfValues.length > 0){
              this.isEmpty = false;
          } else {
            this.isEmpty = true;
          }
          console.log(this.listOfValues);
          this.fulfill = this.responseObj.payload.offering[0].optionalFulfilment;
          console.log('fulfill: ', this.fulfill);
          console.log('ELIGIBLE', this.responseObj.payload.offering[0].OfferingName);
          this.decisionParams = this.responseObj.payload.offering[0].decisionParams;

          // this.speed = _.find(this.responseObj.payload.offering, { eligibleUser: 'Y' , decisionCode: 'TIME_VD'} );
          // this.cost = _.find(this.responseObj.payload.offering, { eligibleUser: 'Y' , decisionCode: 'COST' } );

          console.log(this.decisionParams);
          this.factorscore = this.listOfValues[0].offeringNormalizedScore;
          console.log('factoreSCore:: ', this.factorscore);
          console.log("Emitting the cbxProfileForm Data");
          console.log(this.requestObj);
          this.formEmitter.emit(this.requestObj);
          this.getStarRatingValue(data);
          this.getContextualRefId();
          this.getRecommendedOffering();
        }, error => {
          console.log('CP Call Error');
          console.log(error);
          this.isEmpty = true;
        });
    }
    public setRadioButton(listOfValues: any[]): void {
      this.listOfValues.forEach(element => {
        if(element.recommendation === 'Y') {
          console.log("Setting the value of the radio button");
          this.options = element;
        }
      });
    }

    public getScoreValues(cpResponse: any): number[][]
    {
      console.log("Inside getScoreValues")
      var timeScoreArray: number[] = new Array();
      var costScoreArray: number[] = new Array();
      var scoreArray: number[][] = new Array();
      console.log("CpResponse: ");
      console.log(cpResponse.payload);
      console.log("Offering array");
      console.log(cpResponse.payload.offering);
      cpResponse.payload.offering.forEach(outerElement => {
        console.log("Calculating score values");
        console.log(outerElement);
        console.log(outerElement.decisionParams);
        // if(outerElement.eligible === 'Y'){
        outerElement.decisionParams.forEach(innerElement => {
            console.log("Printing the innerElement");
            console.log(innerElement);
            console.log(innerElement.score);
            console.log(innerElement.decisionCode);
            if(innerElement.decisionCode === 'COST') {
              console.log("The score for cost is: ");
              console.log(innerElement.score);
              costScoreArray.push(innerElement.score);
            }
            if(innerElement.decisionCode === 'TIME_VD') {
              console.log("The score for time is: ");
              console.log(innerElement.score);
              timeScoreArray.push(innerElement.score);
            }
            // timeScoreArray.push();
          });
        // }
      });
      scoreArray.push(timeScoreArray);
      scoreArray.push(costScoreArray);
      console.log("The timescore array is: ");
      console.log(timeScoreArray);
      console.log("The costScoreArray is: ");
      console.log(costScoreArray);
      console.log("The scoreArray is: ");
      console.log(scoreArray);
      console.log("Score value calculation ended");
      return scoreArray;
    }

    public getStarRatingValue(cpResponse: any): void
    {
      console.log("The data obbject received is: ");
      console.log(cpResponse);
      var scoreArray: number[][] = new Array();
      scoreArray = this.getScoreValues(cpResponse);
      console.log("The score array returned is: ");
      console.log(scoreArray);
      console.log("Type of the variable core array is: ");
      console.log(typeof(scoreArray));
      var minMaxArray: number[][] = new Array();
      minMaxArray = this.getMinMaxFromArray(scoreArray);
      console.log("The min max array received is: ");
      console.log(minMaxArray);
      var thresholdArray: number[][] = this.getThresholdsArray(minMaxArray);
      console.log("The threshold array received is: ");
      console.log(thresholdArray);
      var starValueArray: number[][] = this.getStarValuesArray(thresholdArray,scoreArray);
      console.log("The starValueArray received is: ");
      console.log(starValueArray);
      this.invertedStarValue = this.invertStarRatings(starValueArray);
      console.log("The final inverted star values are: ");
      console.log(this.invertedStarValue);
      this.extractEligibleStarValues(this.invertedStarValue);
    }

    public invertStarRatings(starValueArray: number[][]): number[][]
    {
      var invertedStarValue: number[][] = new Array();
      var invertedStarValueAttr: number[];
      for(let outerElement of starValueArray)
      {
        invertedStarValueAttr = new Array();
        for(let innerElement of outerElement)
        {
          invertedStarValueAttr.push(6-(innerElement));
        }
        invertedStarValue.push(invertedStarValueAttr);
      }
      console.log("The inverted star value array being returned is: ");
      console.log(invertedStarValue);
      return invertedStarValue;
    }

    public getStarValuesArray(thresholdsArray: number[][], scoreArray: number[][]): number[][] {
      var starValueArray: number[][] = new Array();
      var starValueAttrArray: number[];
      console.log("The thresholdsArray received inside getStarValuesArray() is: ");
      console.log(thresholdsArray);
      console.log("The scoreArray is: ");
      console.log(scoreArray);
      var outerIndex: number = 0;
      var innerIndex: number = 0;
      var thresholdIndex: number = 0;
      var starRatingVal: number = 0;
      var closestThreshold: number = 0;
      for(let outerElement of scoreArray)
      {
        starValueAttrArray = new Array();
        innerIndex = 0;
        console.log("The outerElement is: ");
        console.log(outerElement);
        for(let innerElement of outerElement)
        {
          console.log("The innerElement is: ");
          console.log(innerElement);
          thresholdIndex = 0;
          for(let thresholdElement of thresholdsArray[outerIndex])
          {
            console.log("The thresholdElement is: ");
            console.log(thresholdElement);
            console.log(typeof(thresholdElement));
            console.log("The scoreElement is: ");
            console.log(innerElement);
            console.log(typeof(innerElement));
            if(+innerElement === thresholdElement) {
              starValueAttrArray.push(thresholdIndex+1);
              console.log("the score is less than equal to threshold. Therefore assigning the star rating as: ");
              console.log(thresholdIndex+1);
              break;
            }else{
              if((+innerElement > thresholdElement) && (+innerElement < thresholdsArray[outerIndex][thresholdsArray[outerIndex].indexOf(thresholdElement)+1]))
              {
                starRatingVal = 0;
                closestThreshold = this.calculateClosestThreshold(+innerElement,thresholdElement,thresholdsArray[outerIndex][thresholdsArray[outerIndex].indexOf(thresholdElement)+1]);
                console.log("The closest threshold calculated is: ");
                console.log(closestThreshold);
                starRatingVal = thresholdsArray[outerIndex].indexOf(closestThreshold)+1;
                console.log("The star rating calculated is: ");
                console.log(starRatingVal);
                starValueAttrArray.push(starRatingVal);
                console.log("The starValueArray is: ");
                console.log(starValueAttrArray);
              }
            }
            thresholdIndex++;
          }
          innerIndex++;
        }
        console.log("The starValueAttrArray is: ");
        console.log(starValueAttrArray);
        starValueArray.push(starValueAttrArray);
        outerIndex++;
      }
      console.log("The starValueArray while returning is: ");
      console.log(starValueArray);
      return starValueArray;
    }

    public calculateClosestThreshold(valueToClassify: number,leftThreshold: number, rightThreshold: number): number {
      console.log("Inside calculateClosestThreshold");
      var leftThresholdDist = valueToClassify - leftThreshold;
      console.log("Left Threshold distance: "+leftThresholdDist);
      var rightThresholdDist = rightThreshold - valueToClassify;
      console.log("Right Threshold distance: "+rightThresholdDist);
      if(leftThresholdDist < rightThresholdDist)
      {
        console.log("leftThresholdDist is less than rightThresholdDist.");
        return leftThreshold;
      }else{
        console.log("leftThresholdDist is not less than rightThresholdDist.");
        return rightThreshold;
      }
    }

    // if((thresholdIndex < (thresholdsArray[outerIndex].length-2)) && (innerElement >= thresholdElement) && (innerElement < thresholdsArray[outerIndex][thresholdIndex+1])) {
    //   // starValueAttrArray.push(thresholdIndex);
    //   starValueAttrArray.push(thresholdIndex+1);
    //   console.log("the score is less than equal to threshold. Therefore assigning the star rating as: ");
    //   console.log(thresholdIndex+1);
    //   break;
    // }else{
    //   console.log("Inside the else block of attr");
    //   console.log("The inner element value is: ");
    //   console.log(innerElement);
    //   console.log("The type of the inner element is: ");
    //   console.log(typeof(innerElement));
    //   console.log("The threshold element is: ");
    //   console.log(thresholdElement);
    //   console.log("The type of the threshold element is: ");
    //   console.log(typeof(thresholdElement));
    //   if(+innerElement === thresholdElement) {
    //     starValueAttrArray.push(5);
    //     console.log("The star value attr array is: ");
    //     console.log(starValueAttrArray);
    //   }
    // }

    public getThresholdsArray(minMaxArray: number[][]): number[][]
    {
      var thresholdArray: number[][] = new Array();
      minMaxArray.forEach(outerElement => {
        var minMaxDiff = (+outerElement[1]) - (+outerElement[0]);
        console.log("The minMaxDiff is: ");
        console.log(minMaxDiff);
        var stepValue = minMaxDiff/4;
        console.log("The stepValue calculated is: ");
        console.log(stepValue);
        var attrThresholdArray: number[] = new Array();
        let i: number = 0;
        var threshold: number = outerElement[0];
        console.log("The threshold initialised is: ");
        console.log(threshold);
        attrThresholdArray.push(outerElement[0]);
        console.log("The attribute wise threshold array on initialisation is: ");
        console.log(attrThresholdArray);
        while(i<4) {
          threshold = threshold + stepValue;
          attrThresholdArray.push(threshold);
          i++;
        }
        console.log("The attribute wise threshold array just after loop is: ");
        console.log(attrThresholdArray);
        console.log("The attribute wise threshold array after everything is: ");
        console.log(attrThresholdArray);
        thresholdArray.push(attrThresholdArray);
        console.log("The entire threshold array is: ");
        console.log(thresholdArray);
      });
      return thresholdArray;
    }

    public extractEligibleStarValues(invertedStarValues: number[][]): void
    {
      var i: number = 0;
      var j: number = 0;
      this.eligibleStarValues = new Array();
      var costArray: number[] = new Array();
      var timeArray: number[] = new Array();
      console.log("listOfvalues: ");
      console.log(this.listOfValues);
      console.log("invertedStrValues: ");
      console.log(this.invertedStarValue);
      for(let offering of this.listOfValues)
      {
        j=0;
        console.log("The offering is: ");
        console.log(offering);
        if(offering.eligible === 'Y')
        {

          for(let decisionParam of offering.decisionParams)
          {
            console.log("The decisionParam is: ");
            console.log(decisionParam);
            console.log("Value of i: ");
            console.log(i);
            console.log("Value of j: ");
            console.log(j);
            if(decisionParam.decisionCode === 'TIME_VD')
            {
              console.log("Inserting the value: "+this.invertedStarValue[j][i]);
              timeArray.push(this.invertedStarValue[j][i]);
              console.log("The time aray now is: ");
              console.log(timeArray);
            }

            if(decisionParam.decisionCode === 'COST')
            {
              console.log("Inserting the value: "+this.invertedStarValue[j][i]);
              costArray.push(this.invertedStarValue[j][i]);
              console.log("The cost aray now is: ");
              console.log(costArray);
            }
            j++;
          }
        }
        i++;
      }
      console.log("the costArray is: ");
      console.log(costArray);
      console.log("the timeArray is: ");
      console.log(timeArray);
      this.eligibleStarValues.push(costArray);
      this.eligibleStarValues.push(timeArray);
      console.log("The eligibleStarVallues are: ");
      console.log(this.eligibleStarValues);

    }

    public populateArrayFromValue(value: number): number[]
    {
      console.log("The value received is: ");
      console.log(value);
      var populatedArrray: number[] = [];
      for(let i=0;i<value;i++)
      {
        populatedArrray.push(1);
      }
      console.log("Returning the array from populateArrayFromValue()");
      console.log(populatedArrray);
      return populatedArrray;
    }

    public getMinMaxFromArray(scoreArray: number[][]): number[][] {
      console.log("Printing the min max array")
      var minMaxArray: number[][] = new Array();
      var index: number = 0;
      var max: number = 0;
      var min: number = 0;
      scoreArray.forEach(outerElement => {
        var decParMinMaxArray: number[] = new Array();
        console.log(outerElement);
        console.log(typeof(outerElement));
        console.log("Printing the data type of the array elements");
        max = +outerElement[0];
        min = +outerElement[0];
        outerElement.forEach(innerElement => {
          console.log(typeof(+innerElement));
          console.log(+innerElement);
          if(+innerElement < min) {
            min = +innerElement;
          }

          if(+innerElement > max) {
            max = +innerElement;
          }
        });
        decParMinMaxArray.push(min);
        decParMinMaxArray.push(max);
        minMaxArray.push(decParMinMaxArray);
      });
      console.log("The min max array being returned is: ");
      console.log(minMaxArray);
      return minMaxArray;
    }

    public getTime(): String {
      let date: Date = new Date();
      console.log("Date: "+date);
      var hours: string = date.getHours().toString();
      var minutes: string = date.getMinutes().toString();
      console.log("The time returned is: "+hours+minutes);
      return hours+minutes;
    }

    public getDate(): String
    {
      let date: Date = new Date();
      console.log("Date: "+date);
      console.log("Date date: "+date.getDate().toPrecision(2));
      var dateDay: string = date.getDate().toString().length <= 1 ? "0"+date.getDate().toString():date.getDate().toString();
      console.log(dateDay);
      console.log("Date month: "+date.getMonth()+1);
      var monthNumber: number = date.getMonth()+1;
      var month: string = monthNumber<=9?"0"+monthNumber.toString():monthNumber.toString();
      console.log("The month is: "+month);
      console.log("Date year: "+date.getFullYear());
      console.log("The date is: "+date.getFullYear()+"-"+month+"-"+dateDay);
      return date.getFullYear()+"-"+month+"-"+dateDay;
    }

    navigationExtras: NavigationExtras = {
      queryParams: {
        "inputFormData": String,
        "offeringRecommended": String,
        "contextualRefId": String,
        "selectedFulfilment": String,
        "notionalRateData": String,
        "loginObject": String
      }
    }

    notionalRateDataObject: any = {
      "instructedAmount": Number,
      "debitAmount": Number,
      "notionalRate": Number,
      "instructedAmountCcy": String,
      "debitAmountCcy": String,
      "CurrentDate":String
    };

    // public saveRecoURL:string = "http://10.196.11.33:14001/igtb/cp/api/savereco";
    reqObj: any = {};
    saveheaderId: any = {};
    public getFulfilment(val : any): void {
      console.log("SaveRecommendation Initiated.....");
      var randomNumber = Math.floor((Math.random()*(999999999-100000000))+100000000);//300346999
      console.log("The random number generated is: "+randomNumber);

      var uniqueno = Math.floor((Math.random()*(999999999-100000000))+100000000);//300346999
      console.log("The uniqueno  generated is: "+randomNumber);

      this.reqObj = {
        'context': {
          'action': 'savereco',
          'channelRequestId': randomNumber.toString(),
          'channelId': 'CBX',
		      'originalChannelRequestId':uniqueno.toString(),
          'userId': 'MKR1'
        },
        'payload': {
          'debtorAccount': this.cbxProfileForm.value.debtorAccount.accNo,
          'debtorAccountBranch': this.cbxProfileForm.value.debtorAccount.branch,
          'debtorAccountCurrency': this.cbxProfileForm.value.debtorAccount.currency,
          'creditorBankCountry': this.cbxProfileForm.value.country.value,
          'creditorName': this.cbxProfileForm.value.contactName.name,
          'customerSegment': 'Business Banking',
          'customerCategory': this.loginObject["category_id"],//'Smart Banking'
          'customerSector': 'AWSE',
          'instructedAmount': this.cbxProfileForm.value.amount,
          'instructedCurrency': this.cbxProfileForm.value.currency,
          'initiationTime': this.getTime(),
          'customerId': this.cbxProfileForm.value.debtorAccount.name,
          'userId': this.loginObject["customer_id"], // USER
          'initiationDate': this.getDate(),
          'valueDate': this.cbxProfileForm.value.receiveOnDate,
          'contact':this.cbxProfileForm.value.contactName.name,
          'offeringCode': this.cbxProfileForm.value.recommendedOfferingCode
        }
      };

      console.log("The reqObj for SaveRecommendation is: ",);
      console.log(this.reqObj);
      console.log("Header Configuration: ");
      this.saveheaderId = uuid();
      console.log("saveheaderId for savereco");
      console.log(this.saveheaderId);
      //const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'AccIess-Control-Allow-Origin': '*', 'X-Request-Id': '21f712dc-43a5-4b0b-b42a-77394e28c00b', 'ChannelId': 'CBX' });
      const myHeaders = new HttpHeaders({ 'Content-Type': 'application/json', 'AccIess-Control-Allow-Origin': '*', 'X-Request-Id': this.saveheaderId, 'ChannelId': 'CBX' });
      console.log("myHeaders",myHeaders);
      console.log("saveRecoURL is: ");
      console.log(this.saveRecoURL);
      this.httpService.post(this.saveRecoURL,
        this.requestObj, {headers: myHeaders}).subscribe(data => {
            console.log("Before savereco data: ");
            console.log(data);
            console.log('SaveRecommendation Call done successfully!');
            console.log('CP Call Success');
            console.log("After savereco data: ");
            console.log(data);
            });

      console.log("The option selected is : ", val);
      console.log(this.options);
      console.log("The fulfilment selected is: ",this.options.DV_VD);
      console.log(this.selectedFulfilment);
      console.log("The cbxProfileForm are: ");
      console.log(this.cbxProfileForm.value.valueDate);

      console.log(this.cbxProfileForm);
      this.notionalRateDataObject["instructedAmount"] = this.cbxProfileForm.value.amount;
      this.notionalRateDataObject["debitAmount"] = this.debitAmount;
      this.notionalRateDataObject["notionalRate"] = this.notionalRate;
      this.notionalRateDataObject["instructedAmountCcy"] = this.cbxProfileForm.value.currency;
      this.notionalRateDataObject["debitAmountCcy"] = this.cbxProfileForm.value.debtorAccount.currency;
      this.notionalRateDataObject["CurrentDate"] = this.options.DV_VD;

      this.navigationExtras.queryParams.inputFormData = JSON.stringify(this.requestObj);
      this.navigationExtras.queryParams.selectedFulfilment = JSON.stringify(this.options);
      this.navigationExtras.queryParams.notionalRateData = JSON.stringify(this.notionalRateDataObject);
      this.navigationExtras.queryParams.contextualRefId = this.getContextualRefId();
      this.navigationExtras.queryParams.offeringRecommended = this.getRecommendedOffering();
      this.navigationExtras.queryParams.loginObject = this.loginObject["customer_id"];
      console.log("The navigation Extras are: ");
      console.log(this.navigationExtras);
      this.router.navigate(['/fulfilment'], this.navigationExtras);
    }

    public getRecommendedOffering(): string {
      console.log("responseObject from recommended offering");
      console.log(this.responseObj);
      var recommendedOfferingCode: string;
      this.responseObj.payload.offering.forEach(element => {
        console.log("element");
        console.log(element);
        //if(element.recommendation === 'Y')
        //{
        recommendedOfferingCode = element.offeringCode;
        console.log('CpEngineURL:....'+ this.cpEngineURL);
        console.log("Gaurav Debugger...."+element);
        console.log("Gaurav Debugger...."+element.recommendation +element.recommendationOrder);
        //}
      })
      console.log("Recommended Offering offering code is: ");
      console.log(recommendedOfferingCode)
      return recommendedOfferingCode;
    }

    public getContextualRefId(): string {
      console.log("responseObject from contextualRef id:");
      console.log(this.responseObj);
      var contextualRefId: string = this.responseObj.context.contextualReferenceId;
      console.log("The contextual RefID is: "+contextualRefId);
      return contextualRefId;
    }

    public notionalRateData = [
      {
        'fromCcy': 'CAD',
        'toCcy': 'INR',
        'exchangeRate':53.3712
      },
      {
        'fromCcy': 'CAD',
        'toCcy': 'USD',
        'exchangeRate':0.7532
      },
      {
        'fromCcy': 'CAD',
        'toCcy': 'AUD',
        'exchangeRate':1.1245
      },
      {
        'fromCcy': 'INR',
        'toCcy': 'CAD',
        'exchangeRate':0.0193
      },
      {
        'fromCcy': 'INR',
        'toCcy': 'USD',
        'exchangeRate':0.0141
      },
      {
        'fromCcy': 'INR',
        'toCcy': 'AUD',
        'exchangeRate':0.0212
      },
      {
        'fromCcy': 'USD',
        'toCcy': 'INR',
        'exchangeRate':71.0123
      },
      {
        'fromCcy': 'USD',
        'toCcy': 'CAD',
        'exchangeRate':1.3301
      },
      {
        'fromCcy': 'USD',
        'toCcy': 'AUD',
        'exchangeRate':1.4802
      },
      {
        'fromCcy': 'AUD',
        'toCcy': 'INR',
        'exchangeRate':48.3423
      },
      {
        'fromCcy': 'AUD',
        'toCcy': 'CAD',
        'exchangeRate':0.8912
      },
      {
        'fromCcy': 'AUD',
        'toCcy': 'USD',
        'exchangeRate':0.6821
      },
    ];

    public getDebitAmount(): void {
      console.log("getDebitAmount called");
      console.log(this.cbxProfileForm);
      this.showCrossCurrencyDiv = false;
      if(((this.cbxProfileForm.value.amount != null)&&(this.cbxProfileForm.value.amount != '')) && ((this.cbxProfileForm.value.currency != null)&&(this.cbxProfileForm.value.currency != '')))
      {
        var payFromAccCurr = this.cbxProfileForm.value.debtorAccount.currency;
        var selectedCurrency = this.cbxProfileForm.value.currency;
        // this.loginUserPasswordArray.forEach(element => {
        //   console.log(element.username);
        //   console.log(element.password);
        //   if(element.username === this.selectedUser) {
        //     this.userRole = element.role;
        //     this.customer_name = element.customer_name;
        //     this.category_name = element.category_name;
        //     console.log("The user role assigned is: "+this.userRole);
        //     this.isUserNameValid = true;
        //   }
        // });
        this.notionalRateData.forEach(element => {
          console.log("payFromCcy: "+payFromAccCurr);
          console.log("selectedCurrency: "+selectedCurrency);
          console.log("fromCcy: "+element.fromCcy);
          console.log("toCccy: "+element.toCcy);
          console.log("exchangeRate: "+element.exchangeRate);
          if((element.fromCcy === selectedCurrency) && (element.toCcy === payFromAccCurr)) {
            this.showCrossCurrencyDiv = true;
            this.notionalRate = element.exchangeRate;
            this.debitAmount = this.cbxProfileForm.value.amount * element.exchangeRate;
            console.log("The debitAmount is: "+this.debitAmount);
            console.log("The notional rate is: "+this.notionalRate);
          } else {
            if(this.showCrossCurrencyDiv !== true) {
              this.showCrossCurrencyDiv = false;
              this.notionalRate = 1;
              this.debitAmount = this.cbxProfileForm.value.amount;
            }
          }
        });
      }
      // this.showCrossCurrencyDiv = true;
    }

    public onChange(value): void {

      this.fromCurrencyType = value.currency;

    }

    public cancelMe(): void {
      window.history.back();
     // this.router.navigate(['/checker-flow'], { state: { initTrans: true } });

    }
public onKnowMore() :void
{

}
    public checkDate(obj,ent) :boolean
    {
      var dateReco =new Date(obj);
      var dateInp =new Date(this.cbxProfileForm.value.receiveOnDate);
      this.ntRecoMes ="";
      if(ent =='Y')
      {
        this.knowMore = true;
        this.ntRecoMes ="This Offering seems a better fit to process the transaction on " + obj+". Would you like to subscribe? ";
        return true;
      }
      if(dateReco.toString() != dateInp.toString())
      {
        this.knowMore = false;
        this.ntRecoMes ="Please note that this offering can process the transaction by " + obj;
        return true;
      }
      return false;

    }



}
