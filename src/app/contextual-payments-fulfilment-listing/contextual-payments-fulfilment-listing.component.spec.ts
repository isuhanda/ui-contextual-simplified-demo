import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualPaymentsFulfilmentListingComponent } from './contextual-payments-fulfilment-listing.component';

describe('ContextualPaymentsFulfilmentListingComponent', () => {
  let component: ContextualPaymentsFulfilmentListingComponent;
  let fixture: ComponentFixture<ContextualPaymentsFulfilmentListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextualPaymentsFulfilmentListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualPaymentsFulfilmentListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
