import { Component, OnInit } from '@angular/core';
import {FulfilmentApiService} from '../services/fulfilmentapi.component';
import { Router, NavigationExtras,ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-contextual-payments-fulfilment-listing',
  templateUrl: './contextual-payments-fulfilment-listing.component.html',
  styleUrls: ['./contextual-payments-fulfilment-listing.component.css']
})
export class ContextualPaymentsFulfilmentListingComponent implements OnInit {

  requestObjs:any;
  listRules: any = [];
  OfferingInDerived = [];
  listOfValues:any = [];
  searchObject:any;
  fullfilmentValues : any = [];
  selectedReference : any;
  validateResponseObj : any = [];
  inputs : string ='I' ;
  searchKey : string;
 showSearchRecord:boolean = false;

   makerValue : string;
   queryCode : string;
  public loginObject: {} = {};
  public loggedInUser:any;
   initTrans : boolean = false;
    constructor(private fullfilmentservice : FulfilmentApiService,private router: Router,private route: ActivatedRoute) {
   // this.editListCodeData();
    this.OfferingInDerived =this.getInputs();
    this.route.queryParams.subscribe(params => {
         console.log("Ankur");
         console.log("Params received are: ");
         console.log(params);
         this.loginObject = JSON.parse(params["loginObject"]);
         //this.inputobjValue = JSON.stringify(params["loginObject"]);
        // console.log(JSON.parse(params["inputobjValue"]));
         console.log("The login object is: ");
        // console.log("...dsfsdfs....",this.inputobjValue);
         console.log(this.loginObject);
        });
        console.log("Login Object");
        console.log(this.loginObject);
    if(this.loginObject != null && this.loginObject["role"]==='MAKER'){

        this.initTrans = true;
        this.makerValue =this.loginObject["customer_id"];
        console.log("MAKER_VALUE FROM CLOSE",this.makerValue);
        this.fullfilmentValues = this.getMakerTransactions();
    }else{
        console.log("checker flow :::: ");
        this.fullfilmentValues = this.getFulfilments();
    }

   }

   navigationExtras: NavigationExtras = {
            queryParams: {
                "selectedKeyObject": String,
                "validFulfilmentsRes":String,
                "txnStatus": String,
                "userRole": String,
                "loginObjectVal":String
            }
        }


   initTransObj: NavigationExtras = {
    queryParams: {
      "initObject": String
      //JSON.stringify(this.loginObject)
    }
  }
  ngOnInit() {


  }






  // async editListCodeData() {
  //   console.log("editListCodeData - clicked:-");
  //   this.listRules = await this.getEditInformation(
  //     "CP_FULLFILMENT_VALUES"
  //   );
  //   }

  //   public getEditInformation(qryCode: string): Promise<any> {
  //   return this.fullfilmentservice
  //     .getFulfilments(qryCode)
  //     .toPromise();
  // }

 getFulfilments() {
   console.log("get FULFILMENTS>>>>>>>>>>");

          const requestObj ={
               "queryCode": "ICP_SP_TRANSCATION"
           }


 this.fullfilmentservice.getSPFullfilments(requestObj).subscribe(
      data=>{
          console.log(data);
           this.listOfValues = data;

      }
    );


    // return [
    //   {id_contextual_refid:'REF300346996',in_inst_amt:1000, in_inst_amt_ccy:'CAD',
    //   offering_code_selected:'Interac e-Transfer',decision_score_display_cost:'15',
    //   decision_score_display_time:'190101',maker_date:'MAKER1',maker_id:190101},

    //   {id_contextual_refid:'REF300346997',in_inst_amt:'1000', in_inst_amt_ccy:'CAD',
    //   offering_code_selected:'Smart Banking',decision_score_display_cost:'25',
    //   decision_score_display_time:'190101',maker_date:'MAKER3',maker_id:190101},

    //   {id_contextual_refid:'REF300346998',in_inst_amt:'1000', in_inst_amt_ccy:'CAD',
    //   offering_code_selected:'Bill Payment',decision_score_display_cost:10,
    //   decision_score_display_time:190101,maker_date:'MAKER2',maker_id:'190101'}
    // ];
  }


onInputSelected(val:any){
    this.showSearchRecord = false;
   if(this.loginObject != null && this.loginObject["role"]==='MAKER'){

   if(val === 'A'){
         console.log("Approved Maker...")
             this.queryCode =  "ICP_SP_MAKER_APPRV_TRANSCATIONS";
   }else if(val === 'R'){
       console.log("Rejected Maker...")
                     this.queryCode =  "ICP_SP_MAKER_RJCTD_TRANSCATIONS";

   }else{
        console.log("Initiated Maker...")

                this.queryCode =  "ICP_SP_MAKER_INIT_TRANSCATIONS";

     }

   }else{
   if(val === 'A'){

         this.requestObjs ={
            "queryCode": "ICP_SP_APPRV_TRANSCATIONS"
        }
   }else if(val === 'R'){
          this.requestObjs ={
                      "queryCode": "ICP_SP_RJCTD_TRANSCATIONS"
                  }
   }else{
          this.requestObjs ={
               "queryCode": "ICP_SP_TRANSCATION"
           }
   }
}

   if(this.loginObject != null && this.loginObject["role"]==='MAKER'){

      this.fullfilmentservice.getSearchObject(this.makerValue,this.queryCode).subscribe(
      data=>{
          console.log("*********************");
          console.log(data);
           this.listOfValues = data;
      });
   }else{
    this.fullfilmentservice.getSPFullfilments(this.requestObjs).subscribe(
      data=>{
          console.log(data);
           this.listOfValues = data;

      });
}
}


onSubmitfulfilment(data1:any){
  console.log("Ankur Madiyan");
   this.selectedReference = data1;
  this.loggedInUser = JSON.parse(sessionStorage.getItem('loggedInUser'));
const requestObj ={

	"context" : {
		"action" : "validatereco",
    "channelRequestId" : "60000001",
    "channelId": "CBX",
        "userId":"MKR1"
	},
	"payload" : {
			"debtorAccount" : this.selectedReference.in_dr_ac,
			"debtorAccountBranch" : "TORONTO",
			"debtorAccountCurrency" : this.selectedReference.in_dr_ac_ccy,
			"creditorBankCountry" : this.selectedReference.in_cr_agt_ctry,
			"customerSegment" : "Business Banking",
            "customerCategory" : this.loggedInUser.category_id,
            "customerSector" : "AWSE",
	        "instructedAmount" : String(this.selectedReference.in_inst_amt),
			"instructedCurrency" : this.selectedReference.in_inst_amt_ccy,
			"initiationTime" : this.getTime(),
			"customerId" : "KIRAN",
			"userId" : this.loggedInUser.customer_id,
	        "initiationDate" : this.getDate(),
	        "offeringCode":this.selectedReference.offering_code_selected,
            "valueDate" : this.selectedReference.in_val_dt
 }

}
//   console.log("Selected Object::::",data);
//  this.router.navigate(['/send-money']);


 console.log("Selected Object::::",this.selectedReference+"......................"+data1);
 console.log(this.selectedReference);
 console.log(data1);
 this.fullfilmentservice.getValidFulfilments(requestObj).subscribe(
       data=>{
          console.log("DATA OBJ:::::",data);
            this.navigationExtras.queryParams.validFulfilmentsRes = JSON.stringify(data.payload.offering[0]);


            console.log("Selected Object::::",data1);
            console.log(data1);
             console.log("validatorObject RESObject::::",JSON.stringify(data.payload.offering[0]) );
             console.log(data.payload.offering[0]);

  this.navigationExtras.queryParams.selectedKeyObject = JSON.stringify(this.selectedReference);
  this.navigationExtras.queryParams.txnStatus = this.inputs;
  this.navigationExtras.queryParams.userRole = this.loginObject["role"] !== null ?  this.loginObject["role"] : '';
  console.log("ROLE IN NAVIGATIONS:::::");
  console.log(this.navigationExtras.queryParams.userRole);
  this.navigationExtras.queryParams.loginObjectVal = JSON.stringify(this.loginObject);
        console.log("this.navigationExtras>>>>");
        console.log(this.navigationExtras);
 this.router.navigate(['/validFulfilments'],this.navigationExtras);
              //console.log("Navigation EXTrAS::::",this.navigationExtras)
     }

    );

}



    getInputs() {
    return [
      { title: 'I', value: 'Initiated' },
      { title: 'A', value: 'Approved' },
      { title: 'R', value: 'Rejected' }

    ];
  }

 searchByRefId(){
     this.showSearchRecord=true;
   //this.searchObject =  this.getSerachedObject(this.searchKey,"ICP_SP_SEARCH_TRANSCATIONS");
  // console.log(this.searchObject);
      this.fullfilmentservice.getSearchObject(this.searchKey,"ICP_SP_SEARCH_TRANSCATIONS").subscribe(
      data=>{
          console.log(data);
           this.searchObject = data;
          console.log("id_contextual_refid::",this.searchObject[0].id_contextual_refid);
      });
 }


  getMakerTransactions(){
    // this.showSearchRecord=true;
   //this.searchObject =  this.getSerachedObject(this.searchKey,"ICP_SP_SEARCH_TRANSCATIONS");
  // console.log(this.searchObject);
      this.fullfilmentservice.getSearchObject(this.makerValue,"ICP_SP_MAKER_INIT_TRANSCATIONS").subscribe(
      data=>{
          console.log("*********************");
          console.log(data);
           this.listOfValues = data;

          //console.log("id_contextual_refid::",this.searchObject[0].id_contextual_refid);
      });
 }



initNewTransaction(){
        console.log(this.loginObject);
        console.log("###################");
         this.initTransObj.queryParams.initObject = JSON.stringify(this.loginObject);
         console.log(this.initTransObj.queryParams.initObject);
         this.router.navigate(['/input'],this.initTransObj);
 }


    // public getSerachedObject(primaryKey: string, qryCode: string):any{

    //     return this.fullfilmentservice.getSearchObject(primaryKey, qryCode);

    //  }

    public getTime(): String {
      let date: Date = new Date();
      console.log("Date: "+date);
      var hours: string = date.getHours().toString();
      var minutes: string = date.getMinutes().toString();
      console.log("The time returned is: "+hours+minutes);
      return hours+minutes;
    }

    public getDate(): String
    {
      let date: Date = new Date();
      console.log("Date: "+date);
      console.log("Date date: "+date.getDate().toPrecision(2));
      var dateDay: string = date.getDate().toString().length <= 1 ? "0"+date.getDate().toString():date.getDate().toString();
      console.log(dateDay);
      console.log("Date month: "+date.getMonth()+1);
      var monthNumber: number = date.getMonth()+1;
      var month: string = monthNumber<=9?"0"+monthNumber.toString():monthNumber.toString();
      console.log("The month is: "+month);
      console.log("Date year: "+date.getFullYear());
      console.log("The date is: "+date.getFullYear()+"-"+month+"-"+dateDay);
      return date.getFullYear()+"-"+month+"-"+dateDay;
    }

}
