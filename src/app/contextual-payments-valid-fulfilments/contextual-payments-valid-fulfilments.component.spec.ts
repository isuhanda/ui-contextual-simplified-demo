import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualPaymentsValidFulfilmentsComponent } from './contextual-payments-valid-fulfilments.component';

describe('ContextualPaymentsValidFulfilmentsComponent', () => {
  let component: ContextualPaymentsValidFulfilmentsComponent;
  let fixture: ComponentFixture<ContextualPaymentsValidFulfilmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextualPaymentsValidFulfilmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualPaymentsValidFulfilmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
