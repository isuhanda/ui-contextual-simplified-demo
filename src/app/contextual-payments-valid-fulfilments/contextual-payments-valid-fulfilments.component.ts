import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { SelectObject } from '../selectedObject';
import { ActivatedRoute } from "@angular/router";
import { FulfilmentApiService } from '../services/fulfilmentapi.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-contextual-payments-valid-fulfilments',
  templateUrl: './contextual-payments-valid-fulfilments.component.html',
  styleUrls: ['./contextual-payments-valid-fulfilments.component.css']
})
export class ContextualPaymentsValidFulfilmentsComponent implements OnInit {

  contactName: any;
  country: any;
  debtorAccount: any;
  amount: any;
  currency: any;
  receiveOnDate: any;
  popupflag: boolean = false;
  popmsg: string;
  decision_score_display_cost: string;
  decision_score_display_time: string;
  valid_decision_score_display_cost: string;
  valid_decision_score_display_time: string;
  fulfilmentfield1: string;
  fulfilmentfield2: string;
  fulfilmentfield3: string;
  fulfilmentfield4: string;
  fulfilmentfield5: string;
  fulfilmentfield6: string;
  fulfilmentfield7: string;
  fulfilmentfield8: string;
  valid_decision_score_cost: number;
  valid_decision_score_time: string;


  public cbxProfileForm: FormGroup;
  //  public validatorObject : ValidatorResObject;
  public cbxInputForm: any;
  public selectedReference: any[] = [];
  public validateResponseObj: any[] = [];
  listOfOffering: any = [];
  offeringflag: string;
  public currentNavigation: any;
  public selectKeyObjects: any;
  public validrespObject: any;
  public fulfilmentfieldsArray: [] = [];
  public txnStatus: any;
  userType: string;
  initTrans: boolean = false;
  public loginObject: Object;
  rejectedComnt: string;
  isRejected: boolean = false;
  public contactNames = [
    {
      'name': 'TBC001',
      'accNo': '97388233998',
      'country': 'USA'
    },
    {
      'name': 'TBC002',
      'accNo': '97388233999',
      'country': 'CA'
    },
    {
      'name': 'TBC003',
      'accNo': '97388233899',
      'country': 'CA'
    },
    {
      'name': 'TBC004',
      'accNo': '97388233990',
      'country': 'CA'
    }
  ];

  public countryList = [
    {
      'name': 'India',
      'value': 'IND'
    },
    {
      'name': 'United States of America',
      'value': 'US'
    },
    {
      'name': 'Canada',
      'value': 'CA'
    },
    {
      'name': 'Australia',
      'value': 'AUS'
    },
    {
      'name': 'South Africa',
      'value': 'SA'
    },
    {
      'name': 'Sweden',
      'value': 'SW'
    },
  ];

  public payFromAccs = [
    {
      'name': 'PayFromAcc1',
      'accNo': '987654321',
      'currency': 'CAD',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc2',
      'accNo': '975319861',
      'currency': 'INR',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc3',
      'accNo': '975319451',
      'currency': 'AUD',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc4',
      'accNo': '975356861',
      'currency': 'CAD',
      'branch': 'Goregaon'
    },
    {
      'name': 'PayFromAcc5',
      'accNo': '975387861',
      'currency': 'USD',
      'branch': 'Goregaon'
    }
  ];

  /*
  public offeringFieldMapping = {
    'PRD_OFRNG_ETRF_001' : ['Mobile Number','Email ID'],
    'PRD_OFRNG_EFT_001': ['Creditor Bank Name','Creditor Bank Type','Creditor Bank Routing No'],
    'PRD_OFRNG_ACH_001': ['Creditor Bank Routing No','Creditor Account Type'],
    'PRD_OFRNG_WIRE_001': ['Creditor ABA Routing No','Creditor Bank Address','Creditor Bank Name','Creditor Name'],
    'PRD_OFRNG_GMT_001': ['SWIFT Code','Creditor Name','Creditor Address']
  }
  */
  public offeringFieldMapping = {
    'PRD_OFRNG_ETRF_001': ['Phone', 'Email ID', 'Remittance Info'],
    'PRD_OFRNG_EFT_001': ['Creditor Bank Name', 'Creditor Bank Routing No', 'Creditor Account No'],
    'PRD_OFRNG_ACH_001': ['Creditor Account Type', 'Creditor Bank Routing No', 'Creditor Account No', 'Remittance Info'],
    'PRD_OFRNG_WIRE_001': ['Creditor Name', 'Creditor Address', 'Creditor Bank Name', 'Creditor Bank Address', 'Creditor BIC Routing No', 'Details of Charges', 'Remittance Info'],
    'PRD_OFRNG_GMT_001': ['SWIFT Code', 'Creditor Name', 'Creditor Address']
  };

  public currencyList = ['CAD', 'INR', 'USD', 'AUD'];
  constructor(private router: Router, private selectedObject: SelectObject, private route: ActivatedRoute, private fullfilmentservice: FulfilmentApiService) {
    console.log("calling CoNstructor;:::::: valid fulfilments");
    this.currentNavigation = this.router.getCurrentNavigation();
    this.route.queryParams.subscribe(params => {
      console.log("PARAMSSSS : PRINTING:::::");
      console.log(params);
      this.selectedObject = JSON.parse(params["selectedKeyObject"]);
      this.validrespObject = JSON.parse(params["validFulfilmentsRes"]);
      // this.txnStatus = JSON.parse(params["txnStatus"]);
      this.txnStatus = params["txnStatus"];
      console.log("TXNNNNNNNNSTATUSSSS", this.txnStatus);
      // this.selectKeyObjects = JSON.stringify(params["selectedKeyObject"]);
      // this.selectedObject = JSON.parse(JSON.stringify(params["selectedKeyObject"])); //selectedKeyObject
      //this.userType = params["userRole"] != null ?  params["userRole"] : '';
      this.userType = params["userRole"] != null ? params["userRole"] : '';
      console.log("USERRRRRRRRR");
      console.log("userTupe :::: ", this.userType);
      this.loginObject = JSON.parse(params["loginObjectVal"]);

    }
    );

  }


  ngOnInit() {
    this.cbxProfileForm = new FormGroup({
      contactName: new FormControl(''),
      country: new FormControl(''),
      debtorAccount: new FormControl(''),
      amount: new FormControl(''),
      currency: new FormControl(''),
      receiveOnDate: new FormControl(''),
      fulfilmentfield1: new FormControl(''),
      fulfilmentfield2: new FormControl(''),
      fulfilmentfield3: new FormControl(''),
      fulfilmentfield4: new FormControl(''),
      fulfilmentfield5: new FormControl(''),
      fulfilmentfield6: new FormControl(''),
      fulfilmentfield7: new FormControl(''),
      fulfilmentfield8: new FormControl('')
    });

    this.getFulfilmentFields();
    this.contactName = this.selectedObject.id_contextual_refid;
    this.country = this.fetchCountryFromCode(this.selectedObject.in_cr_agt_ctry);
    this.debtorAccount = this.selectedObject.in_cust_id;
    this.amount = this.selectedObject.in_inst_amt;
    this.currency = this.selectedObject.in_inst_amt_ccy;
    this.receiveOnDate = this.selectedObject.in_val_dt;
    this.decision_score_display_cost = this.selectedObject.decision_score_display_cost;
    this.decision_score_display_time = this.selectedObject.decision_score_display_time;
    this.fulfilmentfield1 = ((this.selectedObject.ful_field1 !== null) && (this.selectedObject.ful_field1 !== "")) 
    ? this.selectedObject.ful_field1 : '';
    this.fulfilmentfield2 = ((this.selectedObject.ful_field2 !== null) && (this.selectedObject.ful_field2 !== "")) ? this.selectedObject.ful_field2 : "";
    this.fulfilmentfield3 = ((this.selectedObject.ful_field3 !== null) && (this.selectedObject.ful_field3 !== "")) ? this.selectedObject.ful_field3 : "";
    this.fulfilmentfield4 = ((this.selectedObject.ful_field4 !== null) && (this.selectedObject.ful_field4 !== "")) ? this.selectedObject.ful_field4 : "";
    this.fulfilmentfield5 = ((this.selectedObject.ful_field5 !== null) && (this.selectedObject.ful_field5 !== "")) ? this.selectedObject.ful_field5 : "";
    this.rejectedComnt = ((this.selectedObject.rejectcomments !== null) && (this.selectedObject.rejectcomments !== "")) ? this.selectedObject.rejectcomments : "";

    this.constructionResObj();

    this.offeringflag = this.listOfOffering.eligible;


    /*************************************************************************************************** */
    const decisionParamsTimeObj = _.find(this.listOfOffering.decisionParams, { decisionCode: 'TIME_VD' });
    this.valid_decision_score_time = decisionParamsTimeObj.score;

    const decisionParamsCostObj = _.find(this.listOfOffering.decisionParams, { decisionCode: 'COST' });
    this.valid_decision_score_cost = decisionParamsCostObj.score;

    const decisionParamsDisplayTimeObj = _.find(this.listOfOffering.decisionParams, { decisionCode: 'TIME_VD' });
    this.valid_decision_score_display_time = decisionParamsDisplayTimeObj.scoreDisplay;

    const decisionParamsDisplayCostObj = _.find(this.listOfOffering.decisionParams, { decisionCode: 'COST' });
    this.valid_decision_score_display_cost = decisionParamsDisplayCostObj.scoreDisplay;
    /*************************************************************************************************** */

    // this.valid_decision_score_cost = this.listOfOffering.decisionParams[0].score;

    // this.valid_decision_score_time = this.listOfOffering.decisionParams[1].score;

    // this.valid_decision_score_display_cost = this.listOfOffering.decisionParams[0].scoreDisplay;

    // this.valid_decision_score_display_time = this.listOfOffering.decisionParams[1].scoreDisplay;
    /******************************************************************** */

    if ((this.userType !== null && this.userType !== 'MAKER') || this.userType === '') {

      this.showpopup();

    }

    this.cbxProfileForm.disable();
  }

  navigationExtras: NavigationExtras = {
    queryParams: {
      "loginObject": String
    }
  }

  constructionResObj() {

    const respObject = this.validrespObject;
    this.listOfOffering = respObject;

  }

  public getFulfilmentFields(): void {

    var fulfilmentCode = this.selectedObject["offering_code_selected"];

    this.fulfilmentfieldsArray = this.offeringFieldMapping[fulfilmentCode];

    this.cbxProfileForm.controls['fulfilmentfield1'].setValue(this.selectedObject['ful_field1']);
    this.cbxProfileForm.controls['fulfilmentfield2'].setValue(this.selectedObject['ful_field2']);
    this.cbxProfileForm.controls['fulfilmentfield3'].setValue(this.selectedObject['ful_field3']);
    this.cbxProfileForm.controls['fulfilmentfield4'].setValue(this.selectedObject['ful_field4']);
    this.cbxProfileForm.controls['fulfilmentfield5'].setValue(this.selectedObject['ful_field5']);
    this.cbxProfileForm.controls['fulfilmentfield6'].setValue(this.selectedObject['ful_field6']);
    this.cbxProfileForm.controls['fulfilmentfield7'].setValue(this.selectedObject['ful_field7']);
    this.cbxProfileForm.controls['fulfilmentfield8'].setValue(this.selectedObject['ful_field8']);
  }

  public fetchCountryFromCode(code: String): String {

    var countryName: String = "";
    this.countryList.forEach(element => {
      if (element.value === code) {
        countryName = element.name;
      }
    })
    return countryName;
  }

  showpopup() {

    if (this.offeringflag === 'N' && this.txnStatus === 'I') {

      this.popupflag = true;

    } else if ((this.offeringflag === 'Y') && (this.txnStatus === 'I')) {

      if ((this.selectedObject.decision_score_cost !== +this.valid_decision_score_cost) && (this.selectedObject.decision_score_time !== +this.valid_decision_score_time)) {

        this.popupflag = true;
        this.popmsg = "The Value of Cost to Customer for the transaction has changed from " + this.decision_score_display_cost + " to " + this.valid_decision_score_display_cost + " and The The Value Date of the Customer for the transaction has changed from " + this.decision_score_display_time + " to " + this.valid_decision_score_display_time + " Do you want to Approve the transaction?";

      } else if ((this.selectedObject.decision_score_cost !== +this.valid_decision_score_cost)) {

        this.popupflag = true;
        this.popmsg = "The Value of Cost to Customer for the transaction has changed from " + this.decision_score_display_cost + " to " + this.valid_decision_score_display_cost + " Do you want to Approve the transaction?";

      } else {

        if ((this.selectedObject.decision_score_time !== +this.valid_decision_score_time)) {

          this.popupflag = true;
          this.popmsg = "The Value Date of the Customer for the transaction has changed from " + this.decision_score_display_time + " to " + this.valid_decision_score_display_time + " Do you want to Approve the transaction?";

        } else {

          this.popupflag = false;

        }
      }

    } else {

      this.popupflag = false;

    }

  }

  onPopupclick() {
    this.popupflag = false;
  }

  buttonValue: string;
  isRejectSuccessPopupActive: boolean = false;
  isApproveSuccessPopupActive: boolean = false;
  isTechnicalErrorActive: boolean = false;
  rejectComments: string = '';
  rejectTransactionPopup: boolean = false;
  onSubmitSaveData(button) {
    this.rejectTransactionPopup = false;
    var delMap = {};

    this.buttonValue = button.id;

    var data: {} = {
      "SP_TRANSACTION_MASTER": {
        'id_channel_reqid': this.selectedObject.id_channel_reqid,
        'id_contextual_refid': this.selectedObject.id_contextual_refid,
        'in_dr_ac': this.selectedObject.in_dr_ac,
        'in_dr_ac_ccy': this.selectedObject.in_dr_ac_ccy,
        'in_cr_agt_ctry': this.selectedObject.in_cr_agt_ctry,
        'in_val_dt': this.selectedObject.in_val_dt,
        'in_inst_amt': this.selectedObject.in_inst_amt,
        'in_inst_amt_ccy': this.selectedObject.in_inst_amt_ccy,
        'in_debt_amt': this.selectedObject.in_debt_amt,
        'in_debt_amt_ccy': this.selectedObject.in_debt_amt_ccy,
        'fl_ntnl_rate': parseInt(this.selectedObject.fl_ntnl_rate),
        'in_cust_id': this.selectedObject.in_cust_id,
        'offering_code_recommended': this.selectedObject.offering_code_recommended,
        'offering_code_selected': this.selectedObject.offering_code_selected,
        'decision_score_cost': this.selectedObject.decision_score_cost,
        'decision_score_time': this.selectedObject.decision_score_time,
        'decision_score_display_cost': this.selectedObject.decision_score_display_cost,
        'decision_score_display_time': this.selectedObject.decision_score_display_time,
        'ful_field1': ((this.selectedObject.ful_field1 !== null) && (this.selectedObject.ful_field1 !== "") ? this.selectedObject.ful_field1 : ""),
        'ful_field2': ((this.selectedObject.ful_field2 !== null) && (this.selectedObject.ful_field2 !== "") ? this.selectedObject.ful_field2 : ""),
        'ful_field3': ((this.selectedObject.ful_field3 !== null) && (this.selectedObject.ful_field3 !== "") ? this.selectedObject.ful_field3 : ""),
        'ful_field4': ((this.selectedObject.ful_field4 !== null) && (this.selectedObject.ful_field4 !== "") ? this.selectedObject.ful_field4 : ""),
        'ful_field5': ((this.selectedObject.ful_field5 !== null) && (this.selectedObject.ful_field5 !== "") ? this.selectedObject.ful_field5 : ""),
        'maker_id': this.selectedObject.maker_id,
        'MAKER_DATE': this.selectedObject.maker_date,
        'CHECKER_ID': this.selectedObject.checker_id,
        'CHECKER_DATE': this.selectedObject.checker_date,
        'status': this.buttonValue,
        'rejectcomments': this.buttonValue === 'R' ? this.rejectComments : ''
      }
    };
    console.log("this.selectedObject.id_contextual_refid:::", this.selectedObject.id_contextual_refid);
    delMap["SP_TRANSACTION_MASTER"] = { id_contextual_refid: this.selectedObject.id_contextual_refid }

    this.fullfilmentservice.update(data, delMap).subscribe(
      response => {
        console.log("Success!", response);
        if (this.buttonValue === 'R') {
          //these are variables to activate the success msg popups
          this.isRejectSuccessPopupActive = true;
        } else {
          if (this.buttonValue === 'A') {
            //these are variables to activate the success msg popups
            this.isApproveSuccessPopupActive = true;
          } else {
            console.log("None of the actions from reject and approve were selected");
          }
        }

      },
      error => {
        this.isTechnicalErrorActive = true;
        console.error("Error!", error);
      }
    );
  }

  rejectTransaction(): void {
    this.rejectTransactionPopup = true;
  }

  navigateToCheckerFlow(): void {
    this.router.navigate(['/checker-flow']);
  }

  closeWindow() {
    this.initTrans = true;
    console.log("###########3");
    this.navigationExtras.queryParams.loginObject = JSON.stringify(this.loginObject);
    console.log(this.navigationExtras.queryParams.loginObject);
    this.router.navigate(['/checker-flow'], this.navigationExtras);
  }

}

