import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SelectObject {

 constructor(){}
 id_channel_reqid:string;
 id_contextual_refid:string;

in_dr_ac:string;
in_dr_ac_ccy:string;
in_cr_agt_ctry:string;
in_val_dt:string;
in_inst_amt:string;
in_inst_amt_ccy:string;
in_debt_amt:number;
in_debt_amt_ccy:string;
fl_ntnl_rate:string;
in_cust_id:string;

offering_code_recommended :string;
offering_code_selected:string;
decision_score_cost:number;
decision_score_time:number;
decision_score_display_cost:string;
decision_score_display_time:string;

ful_field1:string;
ful_field2 :string;
ful_field3 :string;
ful_field4 : string;
ful_field5 : string; 


maker_id :string;
maker_date :string;
checker_id :string;
checker_date :string;
status :string;
rejectcomments:string;
}