import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contextual-payments-header',
  templateUrl: './contextual-payments-header.component.html',
  styleUrls: ['./contextual-payments-header.component.css']
})
export class ContextualPaymentsHeaderComponent implements OnInit {

  constructor(private location: Location, private router: Router) { }

  ngOnInit() {
  }

  goBack(): void {
    console.log("Going Back");
    this.location.back();
  }

  goForward(): void {
    console.log("Going Forward");
    this.location.forward();
  }

  logout(): void {
    console.log("Routing back to login screen");
    this.router.navigate(['/login']); 
  }
}
