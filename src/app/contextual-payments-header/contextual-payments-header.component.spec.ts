import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextualPaymentsHeaderComponent } from './contextual-payments-header.component';

describe('ContextualPaymentsHeaderComponent', () => {
  let component: ContextualPaymentsHeaderComponent;
  let fixture: ComponentFixture<ContextualPaymentsHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextualPaymentsHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextualPaymentsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
