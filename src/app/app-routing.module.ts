import { ContextualPaymentCashFlowComponent } from './contextual-payment-cash-flow/contextual-payment-cash-flow.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContextualPaymentsLoginComponent } from './contextual-payments-login/contextual-payments-login.component';
import { ContextualPaymentsInputScreenComponent } from './contextual-payments-input-screen/contextual-payments-input-screen.component';
import { ContextualPaymentsFulfilmentComponent } from './contextual-payments-fulfilment/contextual-payments-fulfilment.component';
import { HorizontalTimelineComponent } from './horizontal-timeline/horizontal-timeline.component';
//import { CheckerComponentComponent } from './checker-component/checker-component.component';
import {ContextualPaymentsFulfilmentListingComponent} from './contextual-payments-fulfilment-listing/contextual-payments-fulfilment-listing.component';
import { ContextualPaymentsValidFulfilmentsComponent } from './contextual-payments-valid-fulfilments/contextual-payments-valid-fulfilments.component';
const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: ContextualPaymentsLoginComponent},
  {path: 'input', component: ContextualPaymentsInputScreenComponent},
  {path: 'fulfilment', component: ContextualPaymentsFulfilmentComponent},
  {path: 'timeline', component: HorizontalTimelineComponent},
  {path: 'checker-flow', component: ContextualPaymentsFulfilmentListingComponent},
 {path: 'validFulfilments',component:ContextualPaymentsValidFulfilmentsComponent},
 {path: 'cashFlow',component:ContextualPaymentCashFlowComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
